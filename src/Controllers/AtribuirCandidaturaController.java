package Controllers;

import Lists.ListaEventos;
import Models.AlgoritmoAtribuicao;
import Models.Atribuicao;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Evento;
import Models.FAE;
import UI.AlgoritmosAtribuicao;
import UI.AtribuirCandidaturaUI;
import UIDialogs.IniciarSessaoGUI;
import java.util.ArrayList;
import java.util.List;

public class AtribuirCandidaturaController implements AtribuirCandidaturaUI, Controller {

    /**
     * Lista dos eventos da candidatura.
     */
    private List<Evento> listaEventos;

    /**
     * Centro de Eventos.
     */
    private CentroEventos centroEventos;

    /**
     * Evento da candidatura.
     */
    private Evento evento;

    /**
     * Lista dos FAE da candidatura.
     */
    private List<FAE> listaFAE;

    /**
     * Lista de candidaturas registadas no sistema.
     */
    private List<Candidatura> listaCandidaturas;

    /**
     * Lista de atribuições registadas no sistema.
     */
    private List<Atribuicao> listaAtribuicoes;

    /**
     * Lista dos algoritmos de atribuição registados no sistema.
     */
    private List<AlgoritmoAtribuicao> listaAlgoritmos;

    /**
     * Lista dos algoritmos de atribuição registados no sistema.
     */
    private List<AlgoritmosAtribuicao> lA;

    /**
     * Algoritmo de atribuição de candidaturas.
     */
    private AlgoritmoAtribuicao algoritmo;

    /**
     * Lista dos eventos da candidatura.
     */
    private ListaEventos le;

    /**
     * O algoritmo de atribuição selecionado pelo utilizador.
     */
    private AlgoritmosAtribuicao mecanismoSelecionado;

    /**
     * Construtor do controller atribuir candidatura.
     *
     * @param centroEventos CentroEventos.
     */
    public AtribuirCandidaturaController(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
        this.listaAtribuicoes = new ArrayList<>();
    }

    /**
     * Devolve o nome do algoritmo.
     *
     * @return o nome do algoritmo selecionado
     */
    public String getNomeAlgoritmo() {
        return this.algoritmo.toString();
    }

    /**
     * Apresenta a lista de eventos do organizador.
     * @return
     */
    @Override
    public List<Evento> getListaEventos() {
        this.listaEventos = centroEventos.getListaEventosOrganizadorAtribuicao(IniciarSessaoGUI.SelectedUser);
        if (this.listaEventos.isEmpty()) {
            throw new IllegalAccessError("Não existem eventos na lista pedida");
        }
        le = new ListaEventos(listaEventos);
        return this.listaEventos;
    }

     /**
     * Valida e seleciona o evento.
     * @param evento evento selecionado
     */
    @Override
    public void selectEvento(Evento evento) {
        le.validaEvento(evento);
        this.evento = evento;
    }

     /**
     * Apresenta, ao utilizador, a lista de algoritmos disponíveis no sistema.
     * @return 
     */
    @Override
    public List<AlgoritmoAtribuicao> getListaAlgoritmos() {
        this.listaAlgoritmos = centroEventos.getListaAlgoritmos();
        for(AlgoritmoAtribuicao at : listaAlgoritmos) {
            System.out.println(at.toString());
        }
        return this.listaAlgoritmos;
    }

    /**
     * Apresenta a lista de candidatura registada no sistema.
     * @return 
     */
    @Override
    public List<Candidatura> getListaCandidaturas() {
        this.listaCandidaturas = evento.getListaCandidaturas();
        return this.listaCandidaturas;
    }

    /**
     * Valida e seleciona o algoritmo.
     * @param algoritmo algoritmo selecionado
     */
    @Override
    public void selectAlgoritmo(AlgoritmoAtribuicao algoritmo) {
        this.algoritmo = algoritmo;
    }

    /**
     * Cria a lista de atribuições.
     * @return a lista de atribuicoes
     */
    @Override
    public List<Atribuicao> createListaAtribuicoes() {
        this.listaAtribuicoes = algoritmo.criarAtribuicoes(evento);
        return this.listaAtribuicoes;
    }

    /**
     * Permite validar as atribuições.
     * @return 
     */
    @Override
    public boolean validaAtribuicoes() {
        return evento.validaAtribuicoes(listaAtribuicoes);
    }

    @Override
    public void addListaAtribuicoes() {
        evento.addListaAtribuicoes(listaAtribuicoes);
    }

    /**
     * Define o algoritmo utilizado.
     *
     * @param alg algoritmo.
     */
    public void definirAlgoritmo(AlgoritmosAtribuicao alg) {
        this.mecanismoSelecionado = alg;
        this.listaAtribuicoes.clear();
        this.listaAtribuicoes.addAll(mecanismoSelecionado.atribuir(evento));
        if (this.listaAtribuicoes.isEmpty()) {
            throw new IllegalAccessError("Não existem mais candidaturas para atribuir");
        }
    }
    
    /**
     * Regista as atribuições efetuadas.
     */
    public void registarAtribuicao() {
        for (Atribuicao a : this.listaAtribuicoes) {
            if (a == null || !a.valida()) {
                throw new IllegalAccessError("Atribuição inválida");
            }
            evento.getListaAtribuicoes().addListaAtribuicoes((List<Atribuicao>) a);
        }
    }

    /**
     * Permite obter a lista de FAE.
     * @return lista FAE
     */
    @Override
    public List<FAE> getListaFAE() {
        this.listaFAE = evento.getRegistoFAE().getFAE();
        return this.listaFAE;
    }

    /**
     * Devolve a designação do algoritmo.
     * @return nome algoritmo
     */
    public String getDesignacaoAlgoritmo() {
        return this.algoritmo.toString();
    }

}
