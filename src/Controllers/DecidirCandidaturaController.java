package Controllers;

import Exceptions.IllegalAccessCandidaturasPorAtribuir;
import UIDialogs.IniciarSessaoGUI;
import Lists.ListaAtribuicoes;
import Lists.ListaEventos;
import Models.Atribuicao;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Decisao;
import Models.Evento;
import Models.FAE;
import Models.Utilizador;
import UI.DecidirCandidaturaUI;
import java.util.ArrayList;
import java.util.List;

public class DecidirCandidaturaController implements DecidirCandidaturaUI, Controller {

    /**
     * Centro de Eventos.
     */
    private CentroEventos centroEventos;
    
    /**
     * Lista dos eventos registados no sistema.
     */
    private List<Evento> listaEventos;
    
    /**
     * Evento escolhido.
     */
    private Evento evento;      
    
    /**
     * Lista de candidaturas registadas no sistema.
     */
    private List<Candidatura> listaCandidaturas;
    
    /**
     * Lista de atribuições registadas no sistema.
     */
    private ListaAtribuicoes listaA;
    
    /**
     * Candidatura a ser decidida.
     */
    private Candidatura candidatura;
    
    /**
     * Decisão tomada.
     */
    private Decisao decisao;
    
    /**
     * Utilizador com login feito.
     */
    private Utilizador utilizador;
    
    /**
     * FAE que iniciou o UC. 
     */
    private FAE fae;
    
    /**
     * Lista dos eventos.
     */
    private ListaEventos le;
    
    /**
     * Construtor do controller decidir candidatura.
     * @param centroEventos o centro de eventos.
     * @param utilizador o utilizador.
     */
    public DecidirCandidaturaController(CentroEventos centroEventos, Utilizador utilizador) {
        this.centroEventos = centroEventos;
        this.utilizador = utilizador;
    }
    
    /**
     * Devolve a lista de eventos registada no sistema.
     * @return 
     */
    @Override
    public List<Evento> getListaEventos() {
        listaEventos = centroEventos.getListaEventosFAE(IniciarSessaoGUI.SelectedUser);
        if (listaEventos.isEmpty()) {
            throw new IllegalAccessError("Utilizador não é FAE para nenhum evento");
        }
        return listaEventos;
    }

    /**
     * Permite selecionar o evento.
     * @param e o evento
     */
    @Override
    public void selectEvento(Evento e) {
        this.evento = e;
    }
    
     /**
     * Permite obter a lista de candidaturas do FAE.
     * @return lista de candidaturas
     */
    @Override
    public List<Candidatura> getListaCandidaturasFAE() {
        this.listaCandidaturas = evento.getListaCandidaturasFAE(utilizador);
        if (listaCandidaturas.isEmpty()) {
            throw new IllegalAccessCandidaturasPorAtribuir("De momento, não existem candidaturas por decidir");
        }
        return this.listaCandidaturas;
    }
    
    /**
     * Lista as candidaturas.
     *
     * @return Lista de candidaturas
     */
    public List<Candidatura> listarCandidaturas() {
        return listaCandidaturas;
    }

    /**
     * Permite selecionar a candidatura.
     * @param candidatura candidatura selecionada
     */
    @Override
    public void selectCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }

    /**
     * Permite obter as candidaturas consoante o utilizador.
     */
    public void obterCandidaturas() {
        this.listaA = evento.getListaAtribuicoes();
        this.listaCandidaturas = new ArrayList<>();
        for (Atribuicao a : listaA.getListaAtribuicoes()) {
            if (IniciarSessaoGUI.SelectedUser.getUsername().equals(a.getFAE().getUtilizador().getUsername())) {
                if (!a.getCandidatura().decididaPeloFae(a.getFAE())) {
                    listaCandidaturas.add(a.getCandidatura());
                }
            }
        }
        if (listaCandidaturas.isEmpty()) {
            throw new IllegalAccessCandidaturasPorAtribuir("De momento, não existem candidaturas por decidir");
        }
    }
    
    /**
     * Cria/regista a decisão tomada.
     * @param decisao_boolean decisão tomada
     * @param textoDescritivo texto justificativo
     */
    @Override
    public void createDecisao(boolean decisao_boolean, String textoDescritivo) {
        FAE fae = faeAUsar();
        decisao = candidatura.createDecisao(fae, decisao_boolean, textoDescritivo);
    }

    /**
     * Permite saber qual o FAE a utilizar o sistema.
     * @return 
     */
    public FAE faeAUsar() {
        return evento.getRegistoFAE().obterFAE(utilizador);
    }
    
    /**
     * Permite validar a decisão tomada.
     * @return 
     */
    @Override
    public boolean validaDecisao() {
        return decisao.validaDecisao();
    }

    /**
     * Permite adicionar a decisão tomada relativamente à candidatura.
     */
    @Override
    public void addDecisao() {
        this.candidatura.addDecisao(decisao);
        listaCandidaturas.remove(candidatura);
    }

    /**
     * Verifica se o evento está definido.
     *
     * @return boolean
     */
    public boolean hasEvento() {
        return evento != null;
    }
}
