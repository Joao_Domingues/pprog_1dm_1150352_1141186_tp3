/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Lists.ListaUtilizadores;
import Models.CentroEventos;
import Models.Utilizador;

public class RegistarUtilizadorController {
    
    /**
     * Lista de utilizadores registados.
     */
    private final ListaUtilizadores lstUt;
    
    /**
     * Utilizador.
     */
    private Utilizador utilizador;
    
    /**
     * Método que instancia o controlador para o caso de uso.
     *
     * @param centro centro eventos.
     */
    public RegistarUtilizadorController(CentroEventos centro) {
        lstUt = centro.getRegistoUtilizadores();
    }

    /**
     * Método que cria um novo utilizador.
     */
    public void novoUtilizador() {
        utilizador = lstUt.novoUtilizador();
    }

    /**
     * Método que define os dados do utilizador usando os parâmetros abaixo indicados.
     *
     * @param username username do utilizador.
     * @param password password do utilizador.
     * @param nome nome do utilizador.
     * @param email email do utilizador.
     * @return true se password é validada; false se não valida password
     */
    public boolean setDados(String username, String password, String nome, String email) {
        utilizador.setEmail(email);
        utilizador.setNome(nome);
        utilizador.setUsername(username);
        if (Utilizador.validaPassword(password)) {
            utilizador.setPassword(password);
            return utilizador.valida();
        }
        return false;
    }

    /**
     * Método que regista o utilizador.
     *
     * @return true se regista, false se nao regista
     */
    public boolean registarUtilizador() {
        return lstUt.registarUtilizador(utilizador);
    }

    /**
     * Método que retorna os dados do utilizador.
     *
     * @return dados do utilizador
     */
    public String getDadosUtilizador() {
        return utilizador.getInfo();
    }

    /**
     * Método que retorna o utilizador.
     *
     * @return utilizador
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }
}
