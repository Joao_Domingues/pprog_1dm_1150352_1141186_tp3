package Controllers;

import Lists.ListaCandidaturas;
import Models.Candidatura;
import Models.Evento;
import UI.SubmeterCandidaturaUI;
import java.util.List;
import Models.CentroEventos;
import Models.Utilizador;

public class SubmeterCandidaturaController implements SubmeterCandidaturaUI, Controller {

    /**
     * Centro de Eventos.
     */
    private CentroEventos centroEventos;

    /**
     * Lista de eventos em submissão.
     */
    private List<Evento> listaEventos;

    /**
     * Lista de candidaturas.
     */
    private ListaCandidaturas listaCandidaturas;

    /**
     * Evento escolhido.
     */
    private Evento evento;

    /**
     * Candidatura em causa.
     */
    private Candidatura candidatura;

    /**
     * Utilizador a submeter a candidatura.
     */
    private Utilizador utilizador;

    /**
     * Método que instancia o controlador para o caso de uso.
     * @param ce Centro de Eventos
     */
    public SubmeterCandidaturaController(CentroEventos ce) {
        this.centroEventos = ce;
    }

    /**
     * Permite o retorno do utilizador através do email.
     * @param email o email do utilizador
     */
    @Override
    public void getUtilizador(String email) {
        utilizador = centroEventos.getUtilizadorEmail(email);
    }

    /**
     * Permite obter a lista de eventos registados no sistema.
     * @return 
     */
    @Override
    public List<Evento> getListaEventos() {
        listaEventos = centroEventos.getListaEventosSubmissao();
        return this.listaEventos;
    }

    /**
     * Permite selecionar o evento.
     * @param evento evento selecionado
     */
    @Override
    public void selectEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     * Verifica se o evento está definido.
     * @return 
     */
    public boolean hasEvento() {
        return evento != null;
    }

    /**
     * Método que define os dados da candidatura usando os parâmetros abaixo indicados.
     * @param nome o nome a preencher
     * @param morada a morada a preencher
     * @param telemovel o telemóvel a preencher
     */
    @Override
    public void setDados(String nome, String morada, int telemovel) {
//        candidatura.setNome(nome);
//        candidatura.setMorada(morada);
//        candidatura.setTelemovel(telemovel);
        this.candidatura = new Candidatura(nome, morada, telemovel);
    }
    
    /**
     * Permiter criar uma nova candidatura.
     */
    @Override
    public void addCandidatura() {
        evento.addCandidatura(candidatura);
    }
    
    /**
     * Exibe a candidatura.
     *
     * @return c candidatura
     */
    public Candidatura mostraCandidatura() {
        return candidatura;
    }
}
