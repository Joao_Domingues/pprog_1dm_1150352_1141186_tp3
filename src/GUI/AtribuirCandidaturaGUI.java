/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import UIDialogs.DialogoAtribuicoes;
import Controllers.AtribuirCandidaturaController;
import Models.AlgoritmoAtribuicao;
import Models.CentroEventos;
import UI.AtribuirCandidaturaUI;
import UIDialogs.DialogoEscolhaEventosGUI;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class AtribuirCandidaturaGUI extends JPanel implements CasosUsoGUI {

    /*
     Sempre que for necessário chamar um método do controller, usar    aaUI.nome_do_metodo();  
     */
    private final CentroEventos centroEventos;
    
    private AtribuirCandidaturaUI aaUI;

    /**
     * Lista de algoritmos.
     */
    private JList<AlgoritmoAtribuicao> listaAlgoritmos;
    
    /**
     * Controller do caso de uso.
     */
     private AtribuirCandidaturaController ctrl;
    
    /**
     * Frame principal que contém o painél.
     */
    private final Frame mainFrame;
    
    private JPanel topPanel;
    private JPanel centerPanel;
    private JPanel bottomPanel;
    private JButton btnCancel;
    private JButton btnGenerate;
    
    /**
     * Construtor da Graphical User Interface.
     *
     * @param mainFrame Frame principal.
     * @param cde centro de eventos.
     */
    public AtribuirCandidaturaGUI(Frame mainFrame, CentroEventos cde) {
        this.mainFrame = mainFrame;
        this.centroEventos = cde;

        setupPanel();
        initializeComponents();
    }
    
    /**
     * Setup do painel.
     */
    private void setupPanel() {
        setLayout(new BorderLayout());
        setBackground(JanelaPrincipal.WINDOW_DEFAULT_BACKGROUND_COLOR);
    }

    /**
     * Cria, inicializa e prepara os componentes.
     */
    private void initializeComponents() {
        //Containers of components
        topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        topPanel.setBackground(null);

        centerPanel = new JPanel();
        centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        centerPanel.setBackground(null);

        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        bottomPanel.setBackground(null);

        //Components and Layers
        listaAlgoritmos = new JList<>();
        listaAlgoritmos.setBorder(new TitledBorder("Algoritmos"));
        
        //Model set when ctrl is created
        btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnCancel(event);
            }
        });

        btnGenerate = new JButton("Guardar");
        btnGenerate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnGenerate(event);
            }
        });

        //Adding the Components on the panels
        centerPanel.add(listaAlgoritmos); //VERIFICAR SE ESTÁ CERTO
        bottomPanel.add(btnCancel);
        bottomPanel.add(btnGenerate);

        //Adding the panels/Components on the content pane
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Guarda as atribuições efetuadas.
     * @param event
     */
    private void clickBtnGenerate(ActionEvent event) {
        try {
            AlgoritmoAtribuicao algoritmo = listaAlgoritmos.getSelectedValue();
            ctrl.selectAlgoritmo(algoritmo);
            new DialogoAtribuicoes(mainFrame, "Atribuições", true, ctrl);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Formato inválido", "Erro", JOptionPane.ERROR_MESSAGE);
        } catch (IllegalArgumentException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        } catch (IllegalAccessError e) {
            if (!e.getMessage().equals("Cancelamento por parte do utilizador")) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Cancela o caso de uso.
     *
     * @param event
     */
    private void clickBtnCancel(ActionEvent event) {
        JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_MENU);
    }

    /**
     * Método que inicia o caso de uso.
     */
    @Override
    public void startingUseCase() {
        try {
            ctrl = new AtribuirCandidaturaController(centroEventos);
            new DialogoEscolhaEventosGUI(mainFrame, "Escolher Eventos", true, ctrl);
            
            listaAlgoritmos.setModel(new AbstractListModel<AlgoritmoAtribuicao>() {
                
                List<AlgoritmoAtribuicao> algoritmos = ctrl.getListaAlgoritmos();
                
                @Override
                public int getSize() {
                    return algoritmos.size();
                }

                @Override
                public AlgoritmoAtribuicao getElementAt(int i) {
                    return algoritmos.get(i);
                }
            });
        } catch (IllegalAccessError e) {
            JOptionPane.showMessageDialog(mainFrame, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_MENU);
        }
    }
}
