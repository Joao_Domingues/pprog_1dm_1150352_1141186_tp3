package GUI;

import UIDialogs.DialogoEscolhaEventosGUI;
import UIDialogs.DialogoConfirmacaoGUI;
import Controllers.Controller;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import Controllers.SubmeterCandidaturaController;
import Models.CentroEventos;
import javax.swing.JOptionPane;
import java.awt.Frame;
import java.awt.LayoutManager;
import javax.swing.border.TitledBorder;

public class DialogoSubmeterCandidaturaGUI extends JPanel implements CasosUsoGUI {

    //GLOBAL WINDOW COMPONENTS
    private JPanel centerPanel;
    private JPanel bottomPanel;
    private JPanel westCenterPanel;
    private JPanel eastCenterPanel;
    private JPanel layerName;
    private JPanel layerAddress;
    private JPanel layerCellPhoneNumber;
    private JLabel lblName;
    private JLabel lblAddress;
    private JLabel lblCellPhoneNumber;
    private JTextField tfInputName;
    private JTextField tfInputAddress;
    private JTextField tfInputCellPhoneNumber;
    private JButton btnCancel;
    private JButton btnSubmission;

    /**
     * Controller do caso de uso.
     */
    private SubmeterCandidaturaController ctrl;

    /**
     * Janela principal que contém o painél.
     */
    private final Frame mainFrame;
   
    /**
     * Centro de Eventos.
     */
    private final CentroEventos cde;

    /**
     * Construtor do painel.
     *
     * @param mainFrame janela principal
     * @param cde centro de eventos
     */
    public DialogoSubmeterCandidaturaGUI(Frame mainFrame, CentroEventos cde) {
        this.mainFrame = mainFrame;
        this.cde = cde;

        setupPanel();
        initializeComponents();
    }

    /**
     * Setup do painel.
     */
    private void setupPanel() {
        final int HORIZONTAL_GAP = 10, VERTICAL_GAP = 10;
        setLayout(new BorderLayout(HORIZONTAL_GAP, VERTICAL_GAP));
        setBackground(JanelaPrincipal.WINDOW_DEFAULT_BACKGROUND_COLOR);
    }

    /**
     * Coloca o caso de uso a default.
     */
    public void defaultForm() {
        tfInputName.setText("");
        tfInputAddress.setText("");
        tfInputCellPhoneNumber.setText("");
    }

   /**
     * Inicializa, prepara e adiciona as componentes do caso de uso ao painel.
     */
    private void initializeComponents() {
        //Containers of components
        centerPanel = new JPanel();
        centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        centerPanel.setBackground(null);
        centerPanel.setBorder(new TitledBorder("Formulário da Candidatura"));

        westCenterPanel = new JPanel();
        westCenterPanel.setLayout(new BoxLayout(westCenterPanel, BoxLayout.Y_AXIS));
        westCenterPanel.setBackground(null);

        eastCenterPanel = new JPanel();
        eastCenterPanel.setLayout(new BoxLayout(eastCenterPanel, BoxLayout.Y_AXIS));
        eastCenterPanel.setBackground(null);

        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        bottomPanel.setBackground(null);

        //Components and Layers
        final Dimension DIMENSION_TEXT_FIELDS = new Dimension(140, 30);
        final int HORIZONTAL_GAP = 20, VERTICAL_GAP = 20;
        final LayoutManager LAYOUT_LAYER_FORM = new FlowLayout(FlowLayout.TRAILING, HORIZONTAL_GAP, VERTICAL_GAP);

        layerName = new JPanel(LAYOUT_LAYER_FORM);
        lblName = new JLabel("Nome:");
        tfInputName = new JTextField();
        tfInputName.setPreferredSize(DIMENSION_TEXT_FIELDS);
        layerName.add(lblName);
        layerName.add(tfInputName);
        layerName.setBackground(null);

        layerAddress = new JPanel(LAYOUT_LAYER_FORM);
        lblAddress = new JLabel("Morada:");
        tfInputAddress = new JTextField();
        tfInputAddress.setPreferredSize(DIMENSION_TEXT_FIELDS);
        layerAddress.add(lblAddress);
        layerAddress.add(tfInputAddress);
        layerAddress.setBackground(null);

        layerCellPhoneNumber = new JPanel(LAYOUT_LAYER_FORM);
        lblCellPhoneNumber = new JLabel("Telemovel:");
        tfInputCellPhoneNumber = new JTextField();
        tfInputCellPhoneNumber.setPreferredSize(DIMENSION_TEXT_FIELDS);
        layerCellPhoneNumber.add(lblCellPhoneNumber);
        layerCellPhoneNumber.add(tfInputCellPhoneNumber);
        layerCellPhoneNumber.setBackground(null);

        btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnCancel(event);
            }
        });

        btnSubmission = new JButton("Seguinte");
        btnSubmission.setEnabled(true);
        btnSubmission.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnSubmission(event);
            }
        });

        //Adding the Components on the panels
        westCenterPanel.add(layerName);
        westCenterPanel.add(layerAddress);
        westCenterPanel.add(layerCellPhoneNumber);

//        eastCenterPanel.add(layerButtons);
        centerPanel.add(westCenterPanel);
        centerPanel.add(eastCenterPanel);

        bottomPanel.add(btnCancel);
        bottomPanel.add(btnSubmission);

        //Adding the panels/Components on the content pane
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Submissão da candidatura.
     *
     * @param event
     */
    private void clickBtnSubmission(ActionEvent event) {
        ctrl.setDados(
                tfInputName.getText(),
                tfInputAddress.getText(),
                Integer.parseInt(tfInputCellPhoneNumber.getText()));

        new DialogoConfirmacaoGUI(mainFrame, "Guardar", true, ctrl);
    }

    /**
     * Cancela o caso de uso.
     *
     * @param event
     */
    private void clickBtnCancel(ActionEvent event) {
        JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_MENU);
    }

    /**
     * Método que inicia o caso de uso.
     */
    @Override
    public void startingUseCase() {
        defaultForm();
        try {
            this.ctrl = new SubmeterCandidaturaController(cde);
            ctrl.getListaEventos();

            new DialogoEscolhaEventosGUI(mainFrame, "Escolher Evento", true, (Controller) ctrl);

        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(mainFrame,
                    "Formato inválido(e.g. Telemóvel deve conter apenas números)",
                    "Erro de entrada de dados",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(mainFrame,
                    "Dados inválidos(e.g. " + ex.getMessage() + ")",
                    "Erro de entrada de dados",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
