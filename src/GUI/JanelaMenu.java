/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Models.CentroEventos;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JanelaMenu extends JPanel {

    //GLOBAL WINDOW COMPONENTS
    private JPanel headerPanel;
    private JPanel menuPanel;
    private JPanel bottomPanel;
    private JButton btnAtribuirCandidatura;
    private JButton btnDecidirCandidatura;
    private JButton btnRegistarCandidatura;
    private JLabel lblMessage;
    private JLabel lblCopyright;
    //GLOBAL CONSTANTS
    /**
     * Janela principal que contém o painel.
     */
    private final Frame mainFrame;
    
    /**
     * Centro de eventos.
     */
    private CentroEventos centroEventos;

    /**
     * Construtor do painel.
     *
     * @param mainFrame janela principal
     * @param centroEventos centro de eventos
     */
    public JanelaMenu(Frame mainFrame, CentroEventos centroEventos) {
        this.mainFrame = mainFrame;
        this.centroEventos = centroEventos;

        setupPanel();
        initializeComponents();
    }

    /**
     * Prepara o painél.
     */
    private void setupPanel() {
        setLayout(new BorderLayout());
        setBackground(JanelaPrincipal.WINDOW_DEFAULT_BACKGROUND_COLOR);
    }

    /**
     * Cria, inicializa e prepara os componentes.
     */
    private void initializeComponents() {
        //Inicialize the panels
        headerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        headerPanel.setBackground(null);

        final int HORIZONTAL_GAP = 20, VERTICAL_GAP = 10;
        menuPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, HORIZONTAL_GAP, VERTICAL_GAP));
        menuPanel.setBackground(null);

        bottomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        bottomPanel.setBackground(null);

        //Creating the components
        lblMessage = new JLabel("BEM-VINDO!");
        lblMessage.setFont(new Font(Font.DIALOG, 10, 20));
        lblMessage.setForeground(Color.BLACK);
        lblCopyright = new JLabel("ORGANIZAÇÃO E GESTÃO DE EVENTOS");
        lblCopyright.setFont(new Font(Font.DIALOG, 10, 20));
        lblCopyright.setForeground(Color.RED);

        //Setup each component
        btnAtribuirCandidatura = new JButton("Atribuir Candidatura");
        btnAtribuirCandidatura.setForeground(Color.BLACK);
        btnAtribuirCandidatura.setBackground(Color.LIGHT_GRAY);
        btnAtribuirCandidatura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnAtribuirCandidatura(event);
            }
        });
        btnAtribuirCandidatura.setMnemonic(KeyEvent.VK_A);

        btnDecidirCandidatura = new JButton("Decidir Candidatura");
        btnDecidirCandidatura.setForeground(Color.BLACK);
        btnDecidirCandidatura.setBackground(Color.LIGHT_GRAY);
        btnDecidirCandidatura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnDecidirCandidatura(event);
            }
        });
        btnDecidirCandidatura.setMnemonic(KeyEvent.VK_D);

        btnRegistarCandidatura = new JButton("Registar Candidatura");
        btnRegistarCandidatura.setForeground(Color.BLACK);
        btnRegistarCandidatura.setBackground(Color.LIGHT_GRAY);
        btnRegistarCandidatura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnRegistarCandidatura(event);
            }
        });
        btnRegistarCandidatura.setMnemonic(KeyEvent.VK_R);

        //Adding the Components on the panels
        headerPanel.add(lblMessage);
        headerPanel.add(lblMessage);
        menuPanel.add(btnAtribuirCandidatura);
        menuPanel.add(btnDecidirCandidatura);
        menuPanel.add(btnRegistarCandidatura);
        bottomPanel.add(lblCopyright);

        //Adding the panels/Components on the content pane
        add(headerPanel, BorderLayout.NORTH);
        add(menuPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Inicialization of the use case 3
     *
     * @param event
     */
    
//    private JMenuItem criarItemDecidirCandidatura() { // codigo duplicado! usar criarSubMenuPrincipal2() - João
//        JMenuItem item = new JMenuItem("Decidir Candidatura", KeyEvent.VK_D);
//        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
//        item.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                Evento evento = centroEventos.getListaEventos().get(0); // ???
//                DialogoEscolherCandidaturaGUI dialogoExibirCandidatura = new DialogoEscolherCandidaturaGUI(JanelaMenu.this, centroEventos, evento.getListaFAE().get(0), evento);
//                DialogoEscolherCandidaturaGUI dialogoExibirCandidatura = new DialogoEscolherCandidaturaGUI(JanelaMenu.this, centroEventos, evento.getListaFAE().get(0), evento);
//            }
//        });
//
//        return item;
//    }
    
     /**
     * Inicia o caso de uso AtribuirCandidatura.
     * @param event 
     */
    private void clickBtnAtribuirCandidatura(ActionEvent event) {
        ((CasosUsoGUI) JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_USE_CASE_3)).startingUseCase();
    }

    /**
     * Inicia o caso de uso DecidirCandidatura.
     * @param event
     */
    private void clickBtnDecidirCandidatura(ActionEvent event) {
        ((CasosUsoGUI) JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_USE_CASE_4)).startingUseCase();
    }

    /**
     * Inicializa o caso de uso SubmeterCandidatura.
     * @param event
     */
    private void clickBtnRegistarCandidatura(ActionEvent event) {
        ((CasosUsoGUI) JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_USE_CASE_5)).startingUseCase();
    }
}
