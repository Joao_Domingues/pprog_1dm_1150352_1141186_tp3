/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import UIDialogs.IniciarSessaoGUI;
import Models.CentroEventos;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JPanel;
import Utils.Descodificador;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

public class JanelaPrincipal extends JFrame {
    
    /**
     * Centro de Eventos.
     */
    private final CentroEventos cde;
    
    /**
     * Painel que contém os diversos painéis.
     */
    private static final JPanel CARD_LAYOUT_PANELS_CONTAINER = new JPanel(new CardLayout());
    
    /**
     * Índice do painel Menu.
     */
    public static final int INDEX_PANEL_MENU = 0;
    
    /**
     * Índice do painel do caso de uso 3.
     */
    public static final int INDEX_PANEL_USE_CASE_3 = 1;
    
    /**
     * Índice do painel do caso de uso 4.
     */
    public static final int INDEX_PANEL_USE_CASE_4 = 2;
    
    /**
     * Índice do painel do caso de uso 5.
     */
    public static final int INDEX_PANEL_USE_CASE_5 = 3;
    
    /**
     * Coloca a janela visível.
     */
    private final boolean WINDOW_DEFAULT_IS_VISIBLE = true;
    
    /**
     * Coloca a cor na janela.
     */
    public static final Color WINDOW_DEFAULT_BACKGROUND_COLOR = Color.WHITE;

    /**
     * Construtor da janela principal.
     *
     * @param titleBar título
     * @param visible coloca a janela visível
     * @param cde centro de eventos
     */
    public JanelaPrincipal(String titleBar, boolean visible, CentroEventos cde) {
        super(titleBar);
        this.cde = cde;
//        preSetupFrame();
        initializeComponents();
        posSetupFrame(WINDOW_DEFAULT_IS_VISIBLE);
    }

    /**
     * Construtor da janela principal.
     *
     * @param titleBar título
     * @param cde centro de eventos
     */
    public JanelaPrincipal(String titleBar, CentroEventos cde) {
        super(titleBar);
        this.cde = cde;
        initializeComponents();;
        posSetupFrame(WINDOW_DEFAULT_IS_VISIBLE);
    }

    /**
     * Prepara a janela principal após a inicialização das componentes.
     *
     * @param visible coloca a janel visível.
     */
    private void posSetupFrame(boolean visible) {
        pack();
        setMinimumSize(getSize());
        setLocationRelativeTo(null);
        setVisible(visible);
        //Login
        new IniciarSessaoGUI(this, "Selecione Utilizador", true, cde);
    }

    /**
     * Cria os componentes e adiciona-os ao painel.
     */
    private void initializeComponents() {
        setJMenuBar(new MenuBar(this, cde));

        CARD_LAYOUT_PANELS_CONTAINER.add(new JanelaMenu(this, cde), Integer.toString(INDEX_PANEL_MENU));
        CARD_LAYOUT_PANELS_CONTAINER.add(new AtribuirCandidaturaGUI(this, cde), Integer.toString(JanelaPrincipal.INDEX_PANEL_USE_CASE_3));
        CARD_LAYOUT_PANELS_CONTAINER.add(new DecidirCandidaturaGUI(this, cde), Integer.toString(JanelaPrincipal.INDEX_PANEL_USE_CASE_4));
        CARD_LAYOUT_PANELS_CONTAINER.add(new DialogoSubmeterCandidaturaGUI(this, cde), Integer.toString(JanelaPrincipal.INDEX_PANEL_USE_CASE_5));
        //Add more panels here V (DO NOT CHANGE THE ORDER, because the INDEX_PANEL_USE_CASE_XX)

        add(CARD_LAYOUT_PANELS_CONTAINER);
    }

    /**
     * Altera a Graphical User Interface para o índice pretendido.
     * @param INDEX índice pretendido.
     * @return painel
     */
    public static JPanel changePanel(int INDEX) {
        ((CardLayout) CARD_LAYOUT_PANELS_CONTAINER.
                getLayout()).
                show(CARD_LAYOUT_PANELS_CONTAINER,
                        Integer.
                        toString(INDEX)
                );
        return (JPanel) CARD_LAYOUT_PANELS_CONTAINER.getComponent(INDEX);
    }

   /**
     * Uma forma mais segura de sair do programa: irá perguntar ao utilizador se pretende sair e salvar os dados
     * registados até ao momento.
     *
     * @param cde Centro de Eventos
     */
    public static void exit(CentroEventos cde) {
        boolean confirmacao = JOptionPane.showConfirmDialog(null,
                "Pretende sair do programa?",
                "Confirmar",
                JOptionPane.OK_OPTION) == 0;
        if (confirmacao) {
            Descodificador.writeObject(Descodificador.NAME_OF_DEFAULT_FILE_DATA, cde);
            System.exit(0);
        }
    }  
}
