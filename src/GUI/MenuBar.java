/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import UIDialogs.IniciarSessaoGUI;
import UIDialogs.DialogoEscolhaFicheiro;
import Models.CentroEventos;
import Models.FicheiroCentroEventos;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

public class MenuBar extends JMenuBar {
    
    /**
     * Janela Principal.
     */
    private final Frame mainFrame;
    
    /**
     * Centro de Eventos
     */
    private CentroEventos cde;
    
    /**
     * Escolha do ficheiro.
     */
    private JFileChooser fileChooser;
    
    /**
     * Ficheiro centro de eventos.
     */
    private FicheiroCentroEventos ficheiroCentroEventos;

    /**
     * Mensagem que irá aparecer quando a operação não está disponível.
     */
    private static final String UNSUPORTED_OPERATION_MESSAGE = "Em desenvolvimento";

    /**
     * Construtor da barra de menu.
     * @param mainFrame janela principal
     * @param cde centro de eventos
     */
    public MenuBar(Frame mainFrame, CentroEventos cde) {
        this.mainFrame = mainFrame;
        this.cde = cde;

        add(createFileMenu());
        add(createAccountMenu());
    }

    /**
     * Menu "Ficheiro".
     * @return
     */
    private JMenu createFileMenu() {
        JMenu m = new JMenu("Ficheiro", false);

        m.setMnemonic(KeyEvent.VK_F);

        m.add(createImportFileItem());
        m.add(createExportFileItem());
        m.add(createExitMenuItem());
        return m;
    }

    /**
     * Menu "Conta".
     * @return
     */
    private JMenu createAccountMenu() {
        JMenu m = new JMenu("Conta", false);

        m.setMnemonic(KeyEvent.VK_C);

        m.add(createSwapUser());
        m.add(createAccountDefinitions());
        return m;
    }

    /**
     * Item "Sair".
     * @return
     */
    private JMenuItem createExitMenuItem() {
        JMenuItem i = new JMenuItem("Sair");

        i.setMnemonic(KeyEvent.VK_S);
        i.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));

        i.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JanelaPrincipal.exit(cde);
            }
        });
        return i;
    }

    /**
     * Permite a importação do ficheiro.
     * @return
     */
    private JMenuItem createImportFileItem() {
        JMenuItem i = new JMenuItem("Importar");

        i.setMnemonic(KeyEvent.VK_I);
        i.setAccelerator(KeyStroke.getKeyStroke("ctrl I"));
        i.setToolTipText("Importar as configurações de inicialização");

        i.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int option = JOptionPane.showConfirmDialog(mainFrame,
                        "Ao efetuar a importação de novas configurações, todos os dados guardados até ao momento serão apagados.\n"
                        + "Pretende efetuar a importação?",
                        "Confirmação",
                        JOptionPane.INFORMATION_MESSAGE);
                if (option == 0) {  //YES_OPTION
                    DialogoEscolhaFicheiro fc = new DialogoEscolhaFicheiro();
                    boolean exception;
                    try {
                        exception = false;
                        if (fc.showOpenDialog(mainFrame) == JFileChooser.APPROVE_OPTION) {
                            fc.saveFileInitializer();
                            JOptionPane.showMessageDialog(mainFrame,
                                    "As novas configurações vão ser definidas no próximo arranque do programa.\n"
                                    + "O sistema vai fechar o programa.",
                                    "Informação",
                                    JOptionPane.INFORMATION_MESSAGE);
                            System.exit(3);
                        }
                    } catch (IllegalArgumentException ex) {
                        exception = true;
                        JOptionPane.showMessageDialog(mainFrame, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return i;
    }
    
    /**
     * Permite a exportação dos dados para um ficheiro.
     * @return 
     */
    private JMenuItem createExportFileItem() {
        JMenuItem item = new JMenuItem("Exportar", KeyEvent.VK_E);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                definirFiltroExtensaoBin(fileChooser);

                int resposta = fileChooser.showSaveDialog(MenuBar.this);
                if (resposta == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    if (!file.getName().endsWith(".bin")) {
                        file = new File(file.getPath().trim() + ".bin");
                    }
                    boolean ficheiroGuardado = false;
                    try {
                        ficheiroGuardado = ficheiroCentroEventos.guardar(file.getPath(),cde);
                    } catch (IOException ex) {
                        Logger.getLogger(JanelaMenu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (!ficheiroGuardado) {
                        JOptionPane.showMessageDialog(
                                MenuBar.this,
                                "Impossível gravar o ficheiro: "
                                + file.getPath() + " !",
                                "Exportar",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(
                                MenuBar.this,
                                "Ficheiro gravado com sucesso.",
                                "Exportar",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        });

        return item;
    }

    /**
     * Define a extensão .bin para ler de um ficheiro binário.
     * @param fileChooser JFileChooser
     */    
    private void definirFiltroExtensaoBin(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("bin");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.bin";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }    
    
    /**
     * Permite terminar a sessão.
     * @return
     */
    private JMenuItem createSwapUser() {
        JMenuItem i = new JMenuItem("Terminar Sessão");

        i.setMnemonic(KeyEvent.VK_T);
        i.setAccelerator(KeyStroke.getKeyStroke("ctrl T"));

        i.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                new IniciarSessaoGUI(mainFrame, "Selecione Utilizador", true, cde);
                JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_MENU);
            }
        });
        return i;
    }

    /**
     * Item "Definições".
     * @return
     */
    private JMenuItem createAccountDefinitions() {
        JMenuItem i = new JMenuItem("Definições");

        i.setMnemonic(KeyEvent.VK_D);
        i.setAccelerator(KeyStroke.getKeyStroke("ctrl D"));

        i.setEnabled(false);
        i.setToolTipText(UNSUPORTED_OPERATION_MESSAGE);
        return i;
    }    
}
