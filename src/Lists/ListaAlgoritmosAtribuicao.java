package Lists;

import Models.AlgoritmoAtribuicao;
import Models.AlgoritmoAtribuicaoEquitativa;
import Models.AlgoritmoAtribuicaoExperiencia;
import Models.AlgoritmoAtribuicaoNumeroFAE;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaAlgoritmosAtribuicao implements Serializable {
    
    /**
     * Lista de algoritmos registados no sistema.
     */
    private List<AlgoritmoAtribuicao> listaAlgoritmos;
    
    /**
     * Construtor da lista de algoritmos de atribuição.
     */
    public ListaAlgoritmosAtribuicao() {
        this.listaAlgoritmos = new ArrayList<>();
        addAlgoritmosPreExistentes();
    }
    
    /**
     * Apresenta a lista de algoritmos existentes.
     * @param listaAlgoritmos a lista de algoritmos
     */
    public ListaAlgoritmosAtribuicao(List<AlgoritmoAtribuicao> listaAlgoritmos) {
        this.listaAlgoritmos = listaAlgoritmos;
        addAlgoritmosPreExistentes();
    }
    
    /**
     * Permite o retorno da lista de algoritmos.
     * @return 
     */
    public List<AlgoritmoAtribuicao> getListaAlgoritmos() {
        return this.listaAlgoritmos;
    }
    
    /**
     * Permite modificar a lista de atribuições. 
     * @param listaAlgoritmos 
     */
    public void setListaAtribuicoes(List<AlgoritmoAtribuicao> listaAlgoritmos) {
        this.listaAlgoritmos = listaAlgoritmos;
        addAlgoritmosPreExistentes();
    }
    
    // Métodos
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "ListaAlgoritmosAtribuicao";
    }
    
    /**
     * Adiciona os algoritmos já definidos no sistema a uma nova lista que seja criada.
     */
    public final void addAlgoritmosPreExistentes() {
        this.listaAlgoritmos.add(new AlgoritmoAtribuicaoEquitativa("Algoritmo de Atribuição Equitativa de Candidaturas"));
        this.listaAlgoritmos.add(new AlgoritmoAtribuicaoExperiencia("Algoritmo de Atribuição de Candidaturas por Experiência"));
        this.listaAlgoritmos.add(new AlgoritmoAtribuicaoNumeroFAE("Algoritmo de Atribuição de Candidaturas por Número de FAE"));
    }
}
