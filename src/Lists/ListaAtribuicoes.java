package Lists;

import Models.Atribuicao;
import Models.Candidatura;
import Models.FAE;
import Models.Utilizador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaAtribuicoes implements Serializable {

    /**
     * Lista de atribuições.
     */
    private final List<Atribuicao> listaAtribuicoes;

    /**
     * Construtor da lista de atribuições, sem parâmetros.
     */
    public ListaAtribuicoes() {
        this.listaAtribuicoes = new ArrayList<>();
    }

    /**
     * Construtor da lista de atribuições recebendo, por parâmetro, uma lista de atribuições.
     * @param listaAtribuicoes
     */
    public ListaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        this.listaAtribuicoes = listaAtribuicoes;
    }

    /**
     * Permite retornar a lista de atribuições.
     * @return
     */
    public List<Atribuicao> getListaAtribuicoes() {
        return this.listaAtribuicoes;
    }

    /**
     * Permite retornar a lista de candidaturas do FAE a utilizar a aplicação.
     * @param utilizador FAE a utilizar a aplicação
     * @return
     */
    public List<Candidatura> getListaCandidaturasFAE(Utilizador utilizador) {
        List<Candidatura> listaCandidaturas = new ArrayList<>();
        for (Atribuicao atribuicao : this.listaAtribuicoes) {
            if (atribuicao.getFAE().getUtilizador().equals(utilizador)) {
                listaCandidaturas.add(atribuicao.getCandidatura());
            }
        }
        return listaCandidaturas;
    }

    /**
     * Permite obter a carga de um determinado FAE.
     *
     * @param f FAE
     * @return int carga
     */
    public int cargaDoFae(FAE f) {
        if (!listaAtribuicoes.isEmpty()) {
            for (Atribuicao a : listaAtribuicoes) {
                if (f.getUtilizador().getUsername().equals(a.getFAE().getUtilizador().getUsername())) {
                    f.adicionaCarga();
                }
            }
        }
        return f.getCarga();
    }

    /**
     * Permite validar as atribuições.
     * @param listaAtribuicoes lista de atribuicoes a ser validada
     * @return
     */
    public boolean validaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        for (Atribuicao atribuicao : listaAtribuicoes) {
            if (this.listaAtribuicoes.contains(atribuicao) == false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Permite adicionar uma atribuição à lista de atribuições existentes.
     * @param listaAtribuicoes que vai ser alterada
     */
    public void addListaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        if (!listaAtribuicoes.isEmpty()) {
            for (Atribuicao atribuicao : listaAtribuicoes) {
                if (this.listaAtribuicoes.contains(atribuicao) == false) {
                    this.listaAtribuicoes.add(atribuicao);
                }
            }
        }
    }

    /**
     * Método que regista as atribuições na lista de atribuições.
     * @param la lista de atribuições.
     * @return
     */
    public boolean registarAtribuicoes(List<Atribuicao> la) {
        return listaAtribuicoes.addAll(la);
    }

    /**
     * Verifica se a candidatura já foi atribuida ao FAE.
     * @param f FAE.
     * @param c Candidatura.
     * @return boolean
     */
    public boolean hasAtribuicao(FAE f, Candidatura c) {
        for (Atribuicao a : listaAtribuicoes) {
            if (a.getCandidatura().equals(c)) {
                if (f.getUtilizador().getUsername().equals(a.getFAE().getUtilizador().getUsername())) {
                    return true;
                }
            }
        }
        return false;
    }
}
