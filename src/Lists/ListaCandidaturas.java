
package Lists;

import Models.Candidatura;
import Models.Decisao;
import Models.FAE;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaCandidaturas implements Serializable {
    
    /**
     * Lista de Candidaturas do Evento.
     */
    private List<Candidatura> listaCandidaturas;
    
    /**
     * Cria uma instância da classe ListaCandidaturas, com todos os atributos definidos com os valores por omissão.
     */
    public ListaCandidaturas() {
        this.listaCandidaturas = new ArrayList<>();
    }
    
    /**
     * Cria uma instância da classe ListaCandidaturas, recebendo por parâmetro, a lista de Candidaturas
     * @param listaCandidaturas lista de candidaturas passada por parâmetro
     */
    public ListaCandidaturas(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }
    
    /**
     * Permite aceder à lista de candidaturas.
     * @return lista de candidaturas
     */
    public List<Candidatura> getListaCandidaturas() {
        return this.listaCandidaturas;
    }
    
    /**
     * Permite modificar a lista de candidaturas.
     * @param listaCandidaturas nova lista de candidaturas
     */
    public void setListaCandidaturas(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }
    
    /**
     *
     * @param candidatura
     * @return
     */
//    public boolean validaCandidatura(Candidatura candidatura) {
//        for (Candidatura candidaturaRegistada : this.listaCandidaturas) {
//            if (candidaturaRegistada.getRepresentante().equals(candidatura.getRepresentante())) {
//                return false;
//            }
//        }
//        return true;
//    }
    
    /**
     * Permite registar/adicionar uma candidatura à lista de candidaturas.
     * @param candidatura a adicionar
     * @return 
     */
    public boolean registarCandidatura(Candidatura candidatura) {
        return listaCandidaturas.contains(candidatura)
                ? false
                : listaCandidaturas.add(candidatura);
    }
    
    /**
     * Verifica se a lista de candidaturas contém alguma candidatura submetida pelo utilizador enviado por parâmetro
     * @param utilizador
     * @return 
     */
//    public boolean validaEventoParaCandidatura(Utilizador utilizador) {
//        for(Candidatura candidatura : listaCandidaturas) {
//            if(candidatura.getRepresentante().equals(utilizador)) {
//                return false;
//            }
//        }
//        return true;
//    }
    
    /**
     * Permite criar uma nova candidatura recebendo, por parâmetro, a morada, o nome e o telemóvel.
     * @param morada morada a receber
     * @param nome nome a receber
     * @param telemovel telemóvel a receber
     * @return 
     */
    public Candidatura createCandidatura(String morada, String nome, int telemovel) {
        return new Candidatura(morada, nome, telemovel);
    }
    
    /**
     * Cria uma nova candidatura.
     *
     * @return Candidatura
     */
    public Candidatura novaCandidatura() {
        return new Candidatura();
    }
    
    /**
     * Regista a decisão de numa determinada candidatura.
     * @param c candidatura sobre a qual foi registada uma decisão
     * @param d decisão.
     */
    public void registarDecisao(Candidatura c, Decisao d) {
        listaCandidaturas.get(listaCandidaturas.indexOf(c)).addDecisao(d);
    }
    
    /**
     * Permite definir a decisão sobre uma determianda candidatura.
     * @param f FAE que tomou a decisão
     * @param decisao decisão tomada
     * @param texto texto justificativo
     * @return Decisao
     */
    public Decisao definirDecisao(FAE f, boolean decisao, String texto) {
        return new Decisao(f, decisao, texto);
    }
}
