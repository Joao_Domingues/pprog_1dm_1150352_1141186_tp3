
package Lists;

import Models.Decisao;
import Models.FAE;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaDecisoes implements Serializable {

    /**
     * Lista de decisões registadas no sistema.
     */
    private List<Decisao> listaDecisoes;
    
    /**
     * Construtor da classe ListaDecisoes não recebendo parâmetros.
     */
    public ListaDecisoes() {
        this.listaDecisoes = new ArrayList<>();
    }
    
    /**
     * Construtor da classe ListaDecisoes recebendo, por parâmetro, uma lista de decisões.
     * @param listaDecisoes lista de decisoes recebida por parâmetro
     */
    public ListaDecisoes(List<Decisao> listaDecisoes) {
        this.listaDecisoes = listaDecisoes;
    }

    /**
     * Permite obter a lista de decisões.
     * @return lista de decisoes
     */
    public List<Decisao> getListaDecisoes() {
        return this.listaDecisoes;
    }
    
    /**
     * Permite modificar a lista de decisões.
     * @param listaDecisoes nova lista de decisoes
     */
    public void setListaDecisoes(List<Decisao> listaDecisoes) {
        this.listaDecisoes = listaDecisoes;
    }
    
    /**
     * Permite adicionar uma decisão à lista de decisões.
     * @param decisao decisão a adicionar
     */
    public void addDecisao(Decisao decisao) {
        this.listaDecisoes.add(decisao);
    }
    
    /**
     * Permite criar uma nova decisão.
     * @param decisao decisao tomada
     * @param textoDescritivo texto justificativo indicado
     * @return 
     */
    public Decisao createDecisao(FAE fae, boolean decisao, String textoDescritivo) {
        return new Decisao(fae, decisao, textoDescritivo);
    }
}
