
package Lists;

import Models.Evento;
import Models.FAE;
import Models.Utilizador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaEventos implements Serializable {
    
    /**
     * Lista de Eventos registados no Centro de Eventos.
     */
    private final List<Evento> listaEventos;

    /**
     * Construtor da classe ListaEventos sem parâmetros.
     */
    public ListaEventos() {
        this.listaEventos = new ArrayList<>();
    }
    
    /**
     * Construtor da classe ListaEventos recebendo, por parâmetro, uma lista de eventos.
     * @param listaEventos lista de eventos recebida por parâmetro
     */
    public ListaEventos(List<Evento> listaEventos) {
        this.listaEventos = listaEventos;
    }

    /**
     * Permite obter a lista de eventos.
     * @return lista de eventos
     */
    public List<Evento> getListaEventos() {
        return this.listaEventos;
    }
 
    /**
     * Permite obter a lista de eventos do FAE a utilizar a aplicação.
     * @param utilizador FAE a utilizar a aplicação
     * @return 
     */
    public List<Evento> getListaEventosFAE(Utilizador utilizador) {
        List<Evento> listaEventos = new ArrayList<>();
        for(Evento evento : this.getListaEventos()) {
            for(FAE fae : evento.getListaFAE()) {
                if(fae.getUtilizador().equals(utilizador)) {
                    listaEventos.add(evento);
                }
            }
        }
        return listaEventos;
    }
    
    /**
     * Permite obter todos os eventos que estão em submissão.
     * @return lista de eventos
     */
    public List<Evento> getListaEventosSubmissao() {
        return this.listaEventos;
    }
    
    /**
     * Permite obter a lista de eventos do organizador.
     * @param utilizador organizador a utilizar a aplicação
     * @return 
     */
    public List<Evento> getListaEventosOrganizadorAtribuicao(Utilizador utilizador) {
        List<Evento> listaEventos = new ArrayList<>();
        for (Evento evento : this.getListaEventos()) {
            if (evento.verificaOrganizador(utilizador)) {
                listaEventos.add(evento);
            }
        }
        return listaEventos;
    }
    
    /**
     * Permite registar/adicionar o evento ao sistema.
     * @param e evento a adicionar
     * @return boolean
     */
    public boolean registarEvento(Evento e) {
        return listaEventos.add(e);
    }

    /**
     * Verifica se o evento passado por parâmetro se encontra registado no sistema.
     * @param e evento passado por parâmetro
     */
    public void validaEvento(Evento e) {
        if (!listaEventos.contains(e)) {
            throw new IllegalAccessError("O evento selecionado não se encontra registado no sistema.");
        }
    }
}

