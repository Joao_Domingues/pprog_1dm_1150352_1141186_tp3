/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Models.FAE;
import Models.Utilizador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaFAE implements Serializable {
    
    /**
     * Lista dos FAE registados no sistema.
     */
    private List<FAE> faes;

    /**
     * Construtor da classe ListaFAE sem parâmetros.
     */
    public ListaFAE() {
        this.faes = new ArrayList<>();
    }

    /**
     * Permite obter a lista de FAE.
     * @return lista FAE
     */
    public List<FAE> getFAE() {
        return this.faes;
    }

    /**
     * Permite obter o FAE a partir do utilizador u.
     * @param u o utilizador.
     * @return fae FAE.
     */
    public FAE obterFAE(Utilizador u) {
        for (FAE f : faes) {
            if (f.getUtilizador().getUsername().equals(u.getUsername())) {
                return f;
            }
        }
        throw new IllegalArgumentException("O utilizador não é FAE neste evento.");
    }

    /**
     * Permite registar/adicionar o FAE à lista de FAE.
     * @param fae FAE a ser adicionado
     */
    public void registarFAE(FAE fae) {
        faes.add(fae);
    }

    /**
     * Se o utilizador for FAE do evento a que pertence este container retorna true.
     * @param u utilizador a ser verificado
     * @return boolean
     */
    public boolean isFAE(Utilizador u) {
        for (FAE f : faes) {
            if (f.getUtilizador().getUsername().equals(u.getUsername())) {
                return true;
            }
        }
        return false;
    }
}
