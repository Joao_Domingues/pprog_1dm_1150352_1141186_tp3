
package Lists;

import Models.GestorEventos;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaGestores implements Serializable {
    
    /**
     *  Lista dos Gestores registados no sistema.
     */
    private List<GestorEventos> listaGestores;
    
    /**
     * Construtor da classe ListaGestores sem parâmetros.
     */
    public ListaGestores() {
        this.listaGestores = new ArrayList<GestorEventos>();
    }
    
    /**
     * Construtor da classe ListaGestores recebendo, por parâmetro, uma lista de Gestores.
     * @param listaGestores lista de gestores passada por parâmetro
     */
    public ListaGestores(List<GestorEventos> listaGestores) {
        this.listaGestores = listaGestores;
    }
    
    /**
     * Permite obter a lista de gestores.
     * @return lista gestores
     */
    public List<GestorEventos> getListaGestores() {
        return this.listaGestores;
    }

    /**
     * Permite modificar a lista de gestores.
     * @param listaGestores lista de gestores enviada por parâmetro
     */
    public void setListaGestores(List<GestorEventos> listaGestores) {
        this.listaGestores = listaGestores;
    }
}
