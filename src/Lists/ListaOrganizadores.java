
package Lists;

import Models.Organizador;
import Models.Utilizador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaOrganizadores implements Serializable {
    
    /**
     *  Lista dos Organizadores registados no sistema.
     */
    private List<Organizador> listaOrganizadores;

    /**
     * Construtor da classe ListaOrganizadores sem parâmetros.
     */
    public ListaOrganizadores() {
        this.listaOrganizadores = new ArrayList<>();
    }
    
    /**
     * Construtor da classe ListaOrganizadores recebendo, por parâmetro, uma lista de organizadores.
     * @param listaOrganizadores lista de organizadores passada por parâmetro
     */
    public ListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }
    
    /**
     * Permite obter a lista de organizadores
     * @return lista organizadores
     */
    public List<Organizador> getListaOrganizadores() {
        return this.listaOrganizadores;
    }
    
    /**
     * Permite modificar a lista de organizadores.
     * @param listaOrganizadores lista de organizadores enviada por parâmetro
     */
    public void setListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }
    
    /**
     * Permite adicionar um organizador à lista de organizadores existente.
     * @param o organizador a adicionar
     */
    public boolean registarOrganizador(Organizador o) {
        return listaOrganizadores.add(o);
    }
    
    /**
     * Permite verificar se o utilizador é um organizador.
     * @param utilizador utilizador a ser verificado
     * @return 
     */
    public boolean verificaOrganizador(Utilizador utilizador) {
        for(Organizador organizador : this.listaOrganizadores) {
            if(organizador.getUtilizador().equals(utilizador)) {
                return true;
            }
        }
        return false;
    }
}
