
package Lists;

import Models.Utilizador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaUtilizadores implements Serializable {
    
    /**
     * Lista dos Utilizadores registados no sistema.
     */
    private final List<Utilizador> listaUtilizadores;
    
    /**
     * Construtor da classe ListaUtilizadores sem parâmetros.
    */
    public ListaUtilizadores() {
        this.listaUtilizadores = new ArrayList<>();
    }
    
    /**
     * Construtor da classe ListaUtilizadores recebendo, por parâmetro, uma lista de utilizadores.
     * @param listaUtilizadores lista de utilizadores passada por parâmetro
     */
    public ListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }
    
    /**
     * Método que retorna a lista de utilizadores.
     * @return lista de utilizadores.
    */
    public List<Utilizador> getListaUtilizadores() {
        return this.listaUtilizadores;
    }
    
    /**
     * Permite obter um determinado utilizador através do username.
     * @param username username de um utilizador
     * @return u utilizador que encontrou no sistema
     */
    public Utilizador getUtilizador(String username) {
        for (Utilizador u : listaUtilizadores) {
            if (u.getUsername().equals(username)) {
                return u;
            }
        }
        throw new IllegalArgumentException("Utilizador não está registado no sistema");
    }
   
    /**
     * Permite obter o utilizador através do email.
     * @param email email de um utilizador
     * @return 
     */
    public Utilizador getUtilizadorEmail(String email) {
        for(Utilizador utilizador : this.listaUtilizadores) {
            if(utilizador.getEmail().equals(email)) {
                return utilizador;
            }
        }
        return null;
    }
    
    /**
     * Método que cria um novo utilizador.
     * @return new Utilizador()
    */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }
    
    /**
     * Método que adiciona um utilizador à lista de utilizadores depois de este ter sido validado;
     * @param ut utilizador a adicionar.
     * @return 
    */
    public boolean registarUtilizador(Utilizador ut) {
        return this.listaUtilizadores.add(ut);
    }
    
    /**
     * Método que valida e verifica se o utilizador já existe na lista de utilizadores.
     * @param utilizador utilizador a validar.
     * @return utilizador.valida() && !listaUtilizadores.contains(utilizador);
    */
    private boolean valida(Utilizador utilizador) {
        return utilizador.valida() && !listaUtilizadores.contains(utilizador);
    }
    
    /**
     * Método que compara dois objetos da lista de utilizadores.
     * @param obj objeto a comparar.
     * @return 
    */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ListaUtilizadores) {
            ListaUtilizadores that = (ListaUtilizadores) obj;
            return that.listaUtilizadores.equals(this.listaUtilizadores);
        }
        return false;
    }
}
