
package Models;

import UI.AlgoritmoAtribuicaoUI;
import java.util.List;

public abstract class AlgoritmoAtribuicao implements AlgoritmoAtribuicaoUI{
    
    /**
     * Descrição do algoritmo de atribuição.
     */
    private String descricao;
    
    /**
     * Construtor da classe AlgoritmoAtribuição sem parâmetros.
     */
    public AlgoritmoAtribuicao() {
        this.descricao = "Sem_Descricao";
    }
    
    /**
     * Construtor da classe AlgoritmoAtribuição recebendo, por parâmetro, a descrição.
     * @param descricao descrição do algoritmo
     */
    public AlgoritmoAtribuicao(String descricao) {
        this.descricao = descricao;
    }
    
    /**
     * Permite criar as atribuições a serem efetuadas.
     * @param evento evento sobre o qual vão ser criadas as atribuições
     * @return 
     */
    @Override
    public abstract List<Atribuicao> criarAtribuicoes(Evento evento);
    
    /**
     * Permite modificar a descrição do algoritmo de atribuição.
     * @param descricao nova descrição
     */
    @Override
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Permite obter a descrição do algoritmo de atribuição.
     * @return descricao
     */
    @Override
    public String getDescricao() {
        return this.descricao;
    }
    
    /**
     * Devolve a descrição textual do algoritmo de atribuição.
     * @return descricao
     */
    @Override
    public String toString() {
        return this.descricao;
    }
}
