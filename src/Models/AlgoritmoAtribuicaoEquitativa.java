package Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AlgoritmoAtribuicaoEquitativa extends AlgoritmoAtribuicao implements Serializable {

    /**
     * Construtor da classe AlgoritmoAtribuiçãoEquitativa sem parâmetros.
     */
    public AlgoritmoAtribuicaoEquitativa() {
        super("Algoritmo de Atribuição Equitativa");
    }

    /**
     * Construtor da classe AlgoritmoAtribuiçãoEquitativa recebendo, por
     * parâmetro, a descrição do algoritmo.
     *
     * @param descricao descrição passada por parâmetro
     */
    public AlgoritmoAtribuicaoEquitativa(String descricao) {
        super(descricao);
    }

    /**
     * Este algoritmo tem por base uma distribuição equitativa das candidaturas
     * e FAE
     *
     * @return lista de atribuicoes
     */
    @Override
    public List<Atribuicao> criarAtribuicoes(Evento e) {
        List<Candidatura> listaCandidaturas = new ArrayList(e.getListaCandidaturas());
        List<FAE> listaFAE = new ArrayList(e.getListaFAE());

        int tamanhoNovaLista = lcm(listaFAE.size(), listaCandidaturas.size());
        List<Atribuicao> listaAtribuicoes = new ArrayList<>();

        int cont = 0, contFAE = 0, contCand = 0;

        while (cont < tamanhoNovaLista) {
            if (contFAE >= listaFAE.size()) {
                contFAE = 0;
            }

            if (contCand >= listaCandidaturas.size()) {
                contCand = 0;
            }

            Atribuicao atribuicao = new Atribuicao(listaCandidaturas.get(contCand), listaFAE.get(contFAE));

            if (listaAtribuicoes.contains(atribuicao) == false) {
                listaAtribuicoes.add(atribuicao);
            }

            contFAE++;
            contCand++;
            cont++;
        }
        return listaAtribuicoes;
    }

    /**
     * Cálculo do maior divisor comum entre a e b.
     *
     * @param a variável a
     * @param b vairável b
     * @return
     */
    public static int gcd(int a, int b) {
        while (b > 0) {
            int temp = b;
            b = a % b; // % is remainder
            a = temp;
        }
        return a;
    }

    /**
     * Cálculo do menor múltiplo comum.
     *
     * @param a variável a
     * @param b variável b
     * @return
     */
    public static int lcm(int a, int b) {
        return a * (b / gcd(a, b));
    }
}
