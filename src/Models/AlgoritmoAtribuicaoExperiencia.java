package Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AlgoritmoAtribuicaoExperiencia extends AlgoritmoAtribuicao implements Serializable {

    /**
     * Construtor da classe AlgoritmoAtribuiçãoExperiencia sem parâmetros.
     */
    public AlgoritmoAtribuicaoExperiencia() {
        super("Algoritmo de Atribuição por Experiência");
    }
    
    /**
     * Construtor da classe AlgoritmoAtribuiçãoExperiencia recebendo, por parâmetro, a descrição do algoritmo.
     * @param descricao descrição passada por parâmetro
     */
    public AlgoritmoAtribuicaoExperiencia(String descricao) {
        super(descricao);
    }

    /**
     * Algoritmo de atribuição de candidaturas de forma a que os FAE mais experientes fiquem com mais candidaturas.
     * @param e Evento.
     * @return Lista de atribuicoes
     */
    @Override
    public List<Atribuicao> criarAtribuicoes(Evento e) {
        List<Atribuicao> atribuicoes = new ArrayList<>();
        List<Candidatura> listaCandidaturas = new ArrayList<>(e.getListaCandidaturas());
        List<FAE> listaFAE = new ArrayList<>(e.getListaFAE());
        Comparator criterio = new Comparator() {
            @Override
            public int compare(Object t, Object t1) {
                int r1 = ((FAE) t).getExperiencia();
                int r2 = ((FAE) t1).getExperiencia();

                if (r1 == r2) {
                    return 0;
                } else if (r1 < r2) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };
        for (Candidatura c : listaCandidaturas) {
            for (int i = 0; i < listaFAE.size(); i++) {
                Collections.sort(listaFAE, criterio);
                if (!e.getListaAtribuicoes().hasAtribuicao(listaFAE.get(i), c)) {
                    Atribuicao a = new Atribuicao(c, listaFAE.get(i));
                    atribuicoes.add(a);
                    break;
                }
            }
        }
        return atribuicoes;
    }
}