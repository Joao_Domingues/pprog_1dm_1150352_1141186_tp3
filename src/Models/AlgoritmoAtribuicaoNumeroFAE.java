package Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class AlgoritmoAtribuicaoNumeroFAE extends AlgoritmoAtribuicao implements Serializable {

    /**
     * Construtor da classe AlgoritmoAtribuiçãoNumeroFAE sem parâmetro.
     */
    public AlgoritmoAtribuicaoNumeroFAE() {
        super("Algoritmo de Atribuição por Número de FAE");
    }

    /**
     * Construtor da classe AlgoritmoAtribuiçãoNumeroFAE recebendo, por parâmetro, a descrição do algoritmo.
     * @param descricao descrição passada por parâmetro
     */
    public AlgoritmoAtribuicaoNumeroFAE(String descricao) {
        super(descricao);
    }

    /**
     * Método que permite a atribuição das candidaturas através de um algoritmo que, a partir de um determinado número
     * de FAE executa um processo de iteração com o objetivo de atribuir todas as candidaturas aos FAE,
     * dependendo do número indicado pelo utilizador da aplicação.
     *
     * @return lista de atribuições
     */
    @Override
    public List<Atribuicao> criarAtribuicoes(Evento evento) {
        List<Candidatura> listaCandidaturas = new ArrayList<>(evento.getListaCandidaturas());
        List<FAE> listaFAE = new ArrayList<>(evento.getListaFAE());
        int numeroFAE = obterQuantidadeFAE(), contFAE = 0, contCand = 0;
        List<Atribuicao> listaAtribuicoes = new ArrayList<>();

        while (candidaturasEmFalta(listaAtribuicoes, listaCandidaturas, numeroFAE)) {
            if (contFAE >= listaFAE.size()) {
                contFAE = 0;
            }

            if (contCand >= listaCandidaturas.size()) {
                contCand = 0;
            }

            Candidatura candidatura = listaCandidaturas.get(contCand);
            FAE fae = listaFAE.get(contFAE);

            if (numeroFAEporCandidatura(listaAtribuicoes, candidatura) >= numeroFAE) {

                contCand++;

            } else {

                Atribuicao atribuicao = new Atribuicao(candidatura, fae);

                if (listaAtribuicoes.contains(atribuicao) == false) {
                    listaAtribuicoes.add(atribuicao);
                }

                contFAE++;
                contCand++;
            }
        }

        return listaAtribuicoes;
    }

    /**
     * Permite saber qual o número de FAE pretendido por candidatura.
     * @param listaAtribuicoes lista de atribuicoes
     * @param candidatura candidatura sobre a qual está a ser utilizad a classe
     * @return 
     */ 
    public int numeroFAEporCandidatura(List<Atribuicao> listaAtribuicoes, Candidatura candidatura) {
        int index = 0;

        if (listaAtribuicoes.isEmpty()) {
            return index;
        }
        for (Atribuicao atribuicao : listaAtribuicoes) {
            if (atribuicao.getCandidatura().equals(candidatura)) {
                index++;
            }
        }
        return index;
    }

    /**
     * Permite saber quais as candidaturas que ainda faltam atribuir.
     * @param listaAtribuicoes lista de atribuicoes
     * @param listaCandidaturas lista de candidatura
     * @param numeroFAE numero de FAE pretendido
     * @return 
     */
    public boolean candidaturasEmFalta(List<Atribuicao> listaAtribuicoes, List<Candidatura> listaCandidaturas, int numeroFAE) {
        int index = 0;
        if (listaAtribuicoes.isEmpty()) {
            return true;
        }

        for (Candidatura candidatura : listaCandidaturas) {
            index = 0;
            for (Atribuicao atribuicao : listaAtribuicoes) {
                if (atribuicao.getCandidatura().equals(candidatura)) {
                    index++;
                }
            }
            if (index < numeroFAE) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Atribuição das candidaturas que é feita através de um algoritmo que a
     * partir de um determinado numero de faes executa um processo de iteração
     * com o objetivo de atribuir todas as candidaturas aos faes, dependendo do
     * numero
     *
     * @param e Exposicao
     * @return lista de atribuições
     */
//    @Override
//    public List<Atribuicao> criarAtribuicoes(Evento e) {
//        int qtdFae = obterQuantidadeFAE();
//        List<Atribuicao> atribuicoes = new ArrayList<>();
//        List<Candidatura> candidaturas = new ArrayList<>(e.getListaCandidaturas());
//        List<FAE> faes = new ArrayList<>(e.getListaFAE().getFAE());
//        for (Candidatura c : candidaturas) {
//            int aux = qtdFae;
//            for (FAE f : faes) {
//                if (!e.getListaAtribuicoes().hasAtribuicao(f, c)) {
//                    Atribuicao a = new Atribuicao(c,f);
//                    atribuicoes.add(a);
//                    if (--aux == 0) {
//                        break;
//                    }
//                }
//            }
//        }
//        return atribuicoes;
//    }  
    
    /**
     * Permite obter a quantidade de FAE pretendida através de uma janela popup.
     *
     * @return quantidadeFAE
     */
    private int obterQuantidadeFAE() {
        String output = (String) JOptionPane.showInputDialog(null,
                "Introduza o nº de FAE aos quais pretende atribuir as candidaturas",
                "Escolha", JOptionPane.QUESTION_MESSAGE,
                null,
                null,
                0);
        if (output == null) {
            throw new IllegalAccessError("Cancelamento por parte do utilizador");
        }
        int qtdFae = Integer.parseInt(output);
        if (qtdFae == 0) {
            throw new IllegalArgumentException("Quantidade de FAE inválida");
        }
        return qtdFae;
    }
}