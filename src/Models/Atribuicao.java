
package Models;

import java.io.Serializable;

public class Atribuicao implements Serializable {

    /**
     * Candidatura selecionada pelo algoritmo.
     */
    private Candidatura candidatura;

    /**
     * FAE selecionado pelo algoritmo.
     */
    private FAE fae;

    /**
     * Candidatura selecionada pelo algoritmo, por omissão.
     */
    private final Candidatura CANDIDATURA_POR_OMISSAO = new Candidatura();

    /**
     * FAE selecionado pelo algoritmo, por omissão.
     */
    private final FAE FAE_POR_OMISSAO = new FAE();

    /**
     * Inicia uma instância da classe Atribuicao, sem parâmetros.
     */
    public Atribuicao() {
        this.candidatura = CANDIDATURA_POR_OMISSAO;
        this.fae = FAE_POR_OMISSAO;
        fae.adicionaCarga();
        fae.adicionaExperiencia();
    }

    /**
     * Inicia uma instância da classe Atribuicao, enviando a candidatura e o FAE como argumentos, incrementando a 
     * carga e experiência do respetivo FAE.
     *
     * @param candidatura candidatura enviada
     * @param fae FAE enviado
     */
    public Atribuicao(Candidatura candidatura, FAE fae) {
        setFae(fae);
        setCandidatura(candidatura);
        fae.adicionaCarga();
        fae.adicionaExperiencia();
    }

    /**
     * Acede à candidatura da atribuição.
     *
     * @return candidatura
     */
    public Candidatura getCandidatura() {
        return this.candidatura;
    }

    /**
     * Acede ao FAE da atribuição.
     *
     * @return fae
     */
    public FAE getFAE() {
        return this.fae;
    }

    /**
     * Define o atributo candidatura.
     *
     * @param candidatura candidatura alterada
     */
    public final void setCandidatura(Candidatura candidatura) {
        if (candidatura == null || !candidatura.valida()) {
            throw new IllegalArgumentException("Candidatura Inválida");
        }
        this.candidatura = candidatura;
    }

    /**
     * Define o atributo FAE.
     *
     * @param fae o FAE alterado.
     */
    public final void setFae(FAE fae) {
        if (fae == null || fae.getUtilizador().getUsername().isEmpty()) {
            throw new IllegalArgumentException("FAE Inválido");
        }
        this.fae = fae;
    }

    /**
     * Validação da atribuição - verifica se tanto o FAE como a candidatura existem no sistema.
     * @return boolean
     * @deprecated
     */
    public boolean valida() {
        try {
            setFae(fae);
            setCandidatura(candidatura);
        } catch (IllegalArgumentException ex) {
            return false;
        }
        return true;
    }

    /**
     * Devolve a descrição textual da classe Atribuicao.
     * @return 
     */
    @Override
    public String toString() {
        return "FAE: " + this.fae.toString() + " - Candidatura: " + this.candidatura.toString();
    }
}
