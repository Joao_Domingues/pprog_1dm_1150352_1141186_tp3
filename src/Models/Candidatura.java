package Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Candidatura implements Serializable {
    
    /**
     * Morada da candidatura.
     */
    private String morada;
    
    /**
     * Nome da candidatura.
     */
    private String nome;
    
    /**
     * Telemóvel da candidatura.
     */
    private int telemovel;
    
    /**
     * Lista de FAE registada no sistema.
     */
    private List<FAE> listaFAE;
    
    /**
     * Lista de decisões da candidatura
     */
    private List<Decisao> listaDecisoes;
    
    /**
     * Morada da candidatura por omissão.
     */
    private final String MORADA_POR_OMISSAO = "Sem_Morada";
    
    /**
     * Nome da candidatura por omissão.
     */
    private final String NOME_POR_OMISSAO = "Sem_Nome";
    
    /**
     * Telemóvel da candidatura por omissão.
     */
    private final int TELEMOVEL_POR_OMISSAO = 000000000;

     /**
     * Construtor da classe Candidatura sem parâmetros.
     */
    public Candidatura() {
        this.nome = NOME_POR_OMISSAO;
        this.morada = MORADA_POR_OMISSAO;
        this.telemovel = TELEMOVEL_POR_OMISSAO;
        this.listaDecisoes = new ArrayList<>();
        this.listaFAE = new ArrayList<>();
    }
    
    /**
     * Constrói uma instância de Candidatura recebendo, por parâmetro, o nome, a morada e o número de telemóvel
     * @param nome o nome
     * @param morada a morada
     * @param telemovel o telemóvel
     */
    public Candidatura(String nome, String morada, int telemovel) {
        setNome(nome);
        setMorada(morada);
        setTelemovel(telemovel);
        this.listaDecisoes = new ArrayList<>();
        this.listaFAE = new ArrayList<>();
    }   
    
    /**
     * Constrói uma instância de Candidatura recebendo, por parâmetro, o representante, os dados, a lista de FAE e 
     * a lista de decisões.
     * @param morada morada da candidatura
     * @param nome nome da candidatura
     * @param telemovel telemóvel da candidatura
     * @param listaFAE lista de FAE
     * @param listaDecisoes lista de decisões
     */
    public Candidatura(String nome, String morada, int telemovel, List<FAE> listaFAE, List<Decisao> listaDecisoes) {
        this.nome = nome;
        this.morada = morada;
        this.telemovel = telemovel;
        this.listaFAE = listaFAE;
        this.listaDecisoes = listaDecisoes;
    }

    /**
     * Devolve a morada da candidatura.
     * @return morada 
     */
    public String getMorada() {
        return this.morada;
    }
    
    /**
     * Altera a morada da candidatura.
     * @param morada a nova morada da candidatura.
     */
    public final void setMorada(String morada) {
        if (morada == null || morada.isEmpty()) {
            throw new IllegalArgumentException("Morada inválida");
        } else {
            this.morada = morada;
        }
    }
    
    /**
     * Devolve o telemóvel da candidatura.
     * @return telemóve da candidatura
     */
    public int getTelemovel() {
        return this.telemovel;
    }
    
    /**
     * Altera o número de telemóvel da candidatura.
     * @param telemovel o novo telemóvel da candidatura.
     */
    public final void setTelemovel(int telemovel) {
        if (telemovel > 999999999 || telemovel < 100000000) {
            throw new IllegalArgumentException("Número de telemóvel inválido");
        } else {
            this.telemovel = telemovel;
        }
    }
    
    /**
     * Devolve o nome da candidatura.
     * @return o nome
     */
    public String getNome() {
        return this.nome;
    }
    
    /**
     * Devolve a lista de FAE.
     * @return lista de FAE.
     */
    public List<FAE> getListaFAE() {
        return this.listaFAE;
    }
    
    /**
     * Devolve a lista de decisões.
     * @return lista de decisoes.
     */
    public List<Decisao> getListDecisoes() {
        return this.listaDecisoes;
    }
    
    /**
     * Modifica a lista de FAE.
     * @param listaFAE a nova lista de FAE.
     */
    public void setListaFAE(List<FAE> listaFAE) {
        this.listaFAE = listaFAE;
    }
    
    /**
     * Modifica a lista de decisões.
     * @param listaDecisoes a nova lista de decisões.
     */
    public void setListaDecisoes(List<Decisao> listaDecisoes) {
        this.listaDecisoes = listaDecisoes;
    }
    
    /**
     * Altera o nome da candidatura.
     * @param nome novo nome da candidatura.
     */
    public final void setNome(String nome) {
        if (nome == null || nome.isEmpty()) {
            throw new IllegalArgumentException("Nome inválido");
        } else {
            this.nome = nome;
        }
    }

    /**
     * Adiciona a decisão tomada à lista de decisões.
     * @param decisao decisão tomada.
     */
    public void addDecisao(Decisao decisao) {
        this.listaDecisoes.add(decisao);
    }

    /**
     * Valida os dados da candidatura.
     * @return boolean true/false.
     */
    public boolean validaDadosCandidatura() {
        return morada != null && !morada.isEmpty();
    }
    
    /**
     * Cria a decisão e texto descritivo da mesma.
     * @param fae FAE que procedeu à tomada de decisão
     * @param decisao decisão tomada.
     * @param textoDescritivo texto descritivo criado/adicionado.
     * @return 
     */
    public Decisao createDecisao(FAE fae, boolean decisao, String textoDescritivo) {
        return new Decisao(fae, decisao, textoDescritivo);
    }
    
    /**
     * Verifica se a candidatura foi decidida previamente pelo FAE em questão.
     * @param fae FAE
     * @return boolean
     */
    public boolean decididaPeloFae(FAE fae) {
        if (!listaDecisoes.isEmpty()) {
            for (Decisao decisao : this.listaDecisoes) {
                if (decisao.getFAE().getUtilizador().getUsername().equals(fae.getUtilizador().getUsername())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Valida a candidatura.
     *
     * @return boolean
     * @deprecated
     */
    public boolean valida() {
        try {
            setNome(nome);
            setMorada(morada);
            setTelemovel(telemovel);
        } catch (IllegalArgumentException ex) {
            return false;
        }
        return true;
    }

    /**
     * Devolve a descrição textual da candidatura.
     * @return 
     */
    @Override
    public String toString() {
        return this.getNome();
    }
}
