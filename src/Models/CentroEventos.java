package Models;

import Lists.ListaAlgoritmosAtribuicao;
import Lists.ListaCandidaturas;
import Lists.ListaEventos;
import Lists.ListaGestores;
import Lists.ListaUtilizadores;
import java.io.Serializable;
import java.util.List;

public class CentroEventos implements Serializable {

    /**
     * Lista de eventos registados no Centro de Eventos.
     */
    private final ListaEventos listaEventos;

    /**
     * Lista de Utilizadores registados no Centro de Eventos.
     */
    private ListaUtilizadores listaUtilizadores;

    /**
     * Lista de gestores de eventos registados no Centro de Eventos.
     */
    private ListaGestores listaGestores;
    
    /**
     * Lista de Algoritmos de Atribuição de Candidaturas registados no Centro de Eventos.
     */
    private ListaAlgoritmosAtribuicao listaAlgoritmos;
    
    /**
     * Lista de candidatura registadas no Centro de Eventos.
     */
    private ListaCandidaturas lc;

    /**
     * Inicia uma instância da classe CentroEventos, sem parâmetros.
     */
    public CentroEventos() {
        this.listaEventos = new ListaEventos();
        this.listaUtilizadores = new ListaUtilizadores();
        this.listaGestores = new ListaGestores();
        this.listaAlgoritmos = new ListaAlgoritmosAtribuicao();
    }

    /**
     * Inicia uma instância da classe CentroEventos, enviando a lista de eventos, lista de utilizadores, 
     * lista de gestores e lista de algoritmos como parâmetros.
     * @param listaEventos lista de eventos
     * @param listaUtilizadores lista de utilizadores
     * @param listaGestores lista de gestores
     * @param algoritmos algoritmos
     */
    public CentroEventos(List<Evento> listaEventos, List<Utilizador> listaUtilizadores, List<GestorEventos> listaGestores, List<AlgoritmoAtribuicao> algoritmos) {
        this.listaEventos = new ListaEventos(listaEventos);
        this.listaUtilizadores = new ListaUtilizadores(listaUtilizadores);
        this.listaGestores = new ListaGestores(listaGestores);
        this.listaAlgoritmos = new ListaAlgoritmosAtribuicao(algoritmos);
    }

    /**
     * Retorna a lista de eventos do Centro de Eventos.
     * @return lista de eventos.
     */
    public List<Evento> getListaEventos() {
        return this.listaEventos.getListaEventos();
    }

    /**
     * Retorna a lista de eventos do Centro de Eventos.
     * @return o registo de eventos.
     */
    public ListaEventos getRegistoEventos() {
        return listaEventos;
    }
    
    /**
     * Acede à lista de utilizadores do centro de eventos.
     * @return lista de utilizadores
     */
    public List<Utilizador> getListaUtilizadores() {
        return this.listaUtilizadores.getListaUtilizadores();
    }

    /**
     * Acede à lista de gestores do centro de eventos.
     * @return lista de gestores
     */
    public List<GestorEventos> getListaGestores() {
        return this.listaGestores.getListaGestores();
    }
    
    /**
     * Acede à lista de candidaturas do centro de eventos.
     * @return 
     */
    public ListaCandidaturas getListaCandidaturas() {
        return this.lc;
    }

    /**
     * Acede à lista de algoritmos de atribuição do centro de eventos.
     * @return lista de algoritmos de atribuição
     */
    public List<AlgoritmoAtribuicao> getListaAlgoritmos() {
        return this.listaAlgoritmos.getListaAlgoritmos();
    }
    
    /**
     * Adiciona um algoritmo à lista de algoritmos.
     *
     * @param a algoritmo
     */
        public void addAlgoritmo(AlgoritmoAtribuicao a) {
        this.listaAlgoritmos.getListaAlgoritmos().add(a);
    }
    
    /**
     * Acede à lista de eventos que se encontram dentro do período de submissão de candidaturas.
     * @return lista de eventos em período de submissão
     */
    public List<Evento> getListaEventosSubmissao() {
        return this.listaEventos.getListaEventosSubmissao();
    }

    /**
     * Acede à lista de eventos nos quais o utilizador indicado está definido como FAE.
     * @param utilizador utilizador passado por parâmetro
     * @return lista de eventos onde é fae
     */
    public List<Evento> getListaEventosFAE(Utilizador utilizador) {
        return this.listaEventos.getListaEventosFAE(utilizador);
    }
    
    /**
     * Acede ao utilizador associado ao email indicado.
     * @param email email indicado
     * @return utilizador do email
     */
    public Utilizador getUtilizadorEmail(String email) {
        return this.listaUtilizadores.getUtilizadorEmail(email);
    }
    
    /**
     * Acede à lista de eventos nos quais o utilizador indicado está definido como Organizador e que estejam em 
     * período de atribuição de candidaturas.
     * @param utilizador utilizador indicado
     * @return lista de eventos onde é organizador e que estejam em período de atribuição
     */
    public List<Evento> getListaEventosOrganizadorAtribuicao(Utilizador utilizador) {
        return this.listaEventos.getListaEventosOrganizadorAtribuicao(utilizador);
    }
    
    /**
     * Retorna a lista dos utilizadores.
     *
     * @return lista dos utilizadores
     */
    public ListaUtilizadores getRegistoUtilizadores() {
        return listaUtilizadores;
    }   
}
