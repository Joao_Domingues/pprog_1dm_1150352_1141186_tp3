package Models;

import java.io.Serializable;

public class Decisao implements Serializable {

    /**
     * Funcionário (FAE) que tomou uma decisão sobre as candidaturas.
     */
    private FAE fae;

    /**
     * Decisão tomada sobre candidatura.
     */
    private boolean decisao;

    /**
     * Texto descritivo sobre decisão tomada.
     */
    private String textoDescritivo;

    /**
     * Decisão sobre a candidatura, por omissão.
     */
    private final boolean DECISAO_POR_OMISSAO = false;

    /**
     * Texto descritivo sobre a decisão, por omissão.
     */
    private final String TEXTODESCRITIVO_POR_OMISSAO = "Sem_Texto_Descritivo";

    /**
     * Inicia uma instância da classe Decisao, sem argumentos.
     */
    public Decisao() {
        this.fae = new FAE();
        this.decisao = DECISAO_POR_OMISSAO;
        this.textoDescritivo = TEXTODESCRITIVO_POR_OMISSAO;
    }

    /**
     * Inicia uma instância da classe Decisao, enviando o FAE, a decisão e o texto justificativo por parâmetro.
     *
     * @param fae FAE enviado
     * @param decisao decisao enviada
     * @param textoDescritivo texto justificativo enviado
     */
    public Decisao(FAE fae, boolean decisao, String textoDescritivo) {
        this.fae = fae;
        this.decisao = decisao;
        this.textoDescritivo = textoDescritivo;
    }

    /**
     * Permite obter a decisão tomada.
     *
     * @return decisão tomada
     */
    public boolean getDecisao() {
        return this.decisao;
    }

    /**
     * Acede ao texto justificativo da decisão tomada.
     *
     * @return texto descritivo
     */
    public String getTextoDescritivo() {
        return this.textoDescritivo;
    }

    /**
     * Permite obter o FAE que efetuou a decisão.
     *
     * @return fae que tomou a decisão.
     */
    public FAE getFAE() {
        return fae;
    }

    /**
     * Altera a decisão tomada.
     *
     * @param decisao nova decisao
     */
    public void setDecisao(boolean decisao) {
        this.decisao = decisao;
    }

    /**
     * Define o texto justificativo.
     *
     * @param textoDescritivo texto justificativo indicado/escrito
     */
    public void setTextoDescritivo(String textoDescritivo) {
        this.textoDescritivo = textoDescritivo;
    }

    /**
     * Devolve a descrição textual da decisão tomada.
     *
     * @return
     */
    @Override
    public String toString() {
        if(decisao) {
            return "Aceite";
        }
        return "Recusada";
    }

    /**
     * Verifica se o texto justificativo não é null nem está vazio.
     *
     * @return false se o texto justificativo for null ou estiver vazio; true caso  não seja null e tenha conteúdo
     */
    public boolean validaDecisao() {
        return textoDescritivo != null && !textoDescritivo.isEmpty();
    }
}
