package Models;

import Lists.ListaAtribuicoes;
import Lists.ListaCandidaturas;
import Lists.ListaFAE;
import Lists.ListaOrganizadores;
import java.io.Serializable;
import java.util.List;

public class Evento implements Serializable {

    /**
     * Título do evento.
     */
    private String titulo;

    /**
     * Texto descritivo do evento.
     */
    private String textoDescritivo;

    /**
     * Local onde o evento irá decorrer.
     */
    private String local;

    /**
     * Período do evento.
     */
    private String periodo;

    /**
     * Lista de organizadores do evento.
     */
    private ListaOrganizadores listaOrganizadores;

    /**
     * Lista de candidaturas do evento.
     */
    private ListaCandidaturas listaCandidaturas;

    /**
     * Lista de atribuições FAE-Candidatura.
     */
    private ListaAtribuicoes listaAtribuicoes;

    /**
     * Lista de FAE.
     */
    private ListaFAE listaFAE;

    /**
     * Título do evento, por omissão.
     */
    private final String TITULO_POR_OMISSAO = "Sem_Titulo";

    /**
     * Texto descritivo do evento, por omissão.
     */
    private final String TEXTODESC_POR_OMISSAO = "Sem_Texto_Descritivo";

    /**
     * Local onde o evento irá decorrer, por omissão.
     */
    private final String LOCAL_POR_OMISSAO = "Sem_Local";

    /**
     * Período do evento, por omissão.
     */
    private final String PERIODO_POR_OMISSAO = "Sem_Periodo";

    /**
     * Inicia um instância da classe Evento, sem parâmetros.
     */
    public Evento() {
        this.titulo = TITULO_POR_OMISSAO;
        this.textoDescritivo = TEXTODESC_POR_OMISSAO;
        this.local = LOCAL_POR_OMISSAO;
        this.periodo = PERIODO_POR_OMISSAO;
        this.listaOrganizadores = new ListaOrganizadores();
        this.listaFAE = new ListaFAE();
        this.listaCandidaturas = new ListaCandidaturas();
        this.listaAtribuicoes = new ListaAtribuicoes();
    }

    /**
     * Inicia uma instância da classe Evento, enviando o Título, Texto descritivo, Local, Período e Data limite 
     * para submissão de candidaturas por parâmetro.
     *
     * @param titulo título do evento
     * @param textoDescritivo texto descritivo do evento
     * @param local local do evento
     * @param periodo período do evento
     */
    public Evento(String titulo, String textoDescritivo, String periodo, String local) {
        this.titulo = titulo;
        this.textoDescritivo = textoDescritivo;
        this.local = local;
        setPeriodo(periodo);
        this.listaOrganizadores = new ListaOrganizadores();
        this.listaFAE = new ListaFAE();
        this.listaCandidaturas = new ListaCandidaturas();
        this.listaAtribuicoes = new ListaAtribuicoes();
    }

    /**
     * Permite obter o título do evento.
     *
     * @return titulo do evento
     */
    public String getTitulo() {
        return this.titulo;
    }

    /**
     * Permite obter o texto descritivo do evento.
     *
     * @return textoDescritivo do evento
     */
    public String getTextoDescritivo() {
        return this.textoDescritivo;
    }

    /**
     * Permite obter o local do evento.
     *
     * @return local do evento
     */
    public String getLocal() {
        return this.local;
    }

    /**
     * Permite obter o período do evento.
     *
     * @return periodo do evento.
     */
    public String getPerido() {
        return this.periodo;
    }

    /**
     * Permite obter a lista de organizadores do evento.
     *
     * @return listaOrganizadores do evento
     */
    public ListaOrganizadores getListaOrganizadores() {
        return this.listaOrganizadores;
    }

    /**
     * Permite obter a lista de FAE do evento.
     *
     * @return listaFAE do evento
     */
    public List<FAE> getListaFAE() {
        return listaFAE.getFAE();
    }

    /**
     * Permite obter a lista de FAE.
     *
     * @return lf lista de FAE
     */
    public ListaFAE getRegistoFAE() {
        return listaFAE;
    }

    /**
     * Permite obter a lista de candidaturas do evento.
     *
     * @return listaCandidaturas do evento
     */
    public List<Candidatura> getListaCandidaturas() {
        return this.listaCandidaturas.getListaCandidaturas();
    }

    /**
     * Permite obter a lista de candidaturas.
     *
     * @return lc - lista de candidaturas
     */
    public ListaCandidaturas getRegistoCandidaturas() {
        return listaCandidaturas;
    }

    /**
     * Permite obter a lista de atribuições do evento.
     *
     * @return listaAtribuicoes do evento
     */
    public ListaAtribuicoes getListaAtribuicoes() {
        return this.listaAtribuicoes;
    }

    /**
     * Permite obter a lista de candidaturas atribuídas ao FAE indicado.
     *
     * @param utilizador FAE indicado
     * @return candidaturas atribuídas
     */
    public List<Candidatura> getListaCandidaturasFAE(Utilizador utilizador) {
        return this.listaAtribuicoes.getListaCandidaturasFAE(utilizador);
    }

    /**
     * Define o título do evento.
     *
     * @param titulo titulo a definir
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Define o texto descritivo do evento.
     *
     * @param textoDescritivo textoDescritivo a definir
     */
    public void setTextoDescritivo(String textoDescritivo) {
        this.textoDescritivo = textoDescritivo;
    }

    /**
     * Define o local do evento.
     *
     * @param local o local a definir
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * Define o periodo do evento.
     *
     * @param periodo periodo do evento a definir
     */
    public void setPeriodo(String periodo) {
        periodo.replace("periodo=", "").trim();
        String[] valoresString = periodo.split("-");
        Integer[] valores = new Integer[valoresString.length];
        int i = 0;
        for (String str : valoresString) {
            valores[i] = Integer.parseInt(str.trim());
            i++;
        }
        this.periodo = "Data Inicio:" + valores[0] + "/" + valores[1] + "/" + valores[2] + "  -  Data Fim: " +  valores[3] + "/" + valores[4] + "/" + valores[5];
    }

    /**
     * Define a lista de organizadores do evento.
     *
     * @param listaOrganizadores a listaOrganizadores a definir
     */
    public void setListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores.setListaOrganizadores(listaOrganizadores);
    }

    /**
     * Define a lista de FAE do evento.
     *
     * @param listaFAE a listaFAE a definir
     */
    public void setListaFAE(ListaFAE listaFAE) {
        this.listaFAE = listaFAE;
    }

    /**
     * Define a lista de candidaturas do evento.
     *
     * @param listaCandidaturas a listaCandidaturas a definir
     */
    public void setListaCandidaturas(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas.setListaCandidaturas(listaCandidaturas);
    }
    
    /**
     * Adiciona a candidatura à lista de candidaturas do evento.
     *
     * @param candidatura candidatura a adicionar.
     */
    public void addCandidatura(Candidatura candidatura) {
        this.listaCandidaturas.registarCandidatura(candidatura);
    }

    /**
     * Verifica se o utilizador já está definido como organizador no evento.
     *
     * @param utilizador utilizador a ser verificado
     * @return true se o utilizador não estiver definido como organizador
     */
    public boolean verificaOrganizador(Utilizador utilizador) {
        return this.listaOrganizadores.verificaOrganizador(utilizador);
    }

    /**
     * Verifica se a atribuição indicada já existe na lista de atribuições do evento.
     *
     * @param listaAtribuicoes lista de atribuicoes a ser verificada
     * @return true se a atribuição for única
     */
    public boolean validaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        return this.listaAtribuicoes.validaAtribuicoes(listaAtribuicoes);
    }

    /**
     * Adiciona as atribuições contidas na lista à lista de atribuições do evento.
     *
     * @param listaAtribuicoes lista de atribuições a ser adicionada
     */
    public void addListaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        this.listaAtribuicoes.addListaAtribuicoes(listaAtribuicoes);
    }

    /**
     * Permite criar uma candidatura ao evento enviando a morada, o nome e telemóvel por parâmetro.
     *
     * @param morada morada
     * @param nome nome
     * @param telemovel telemóvel
     * @return
     */
    public Candidatura createCandidatura(String morada, String nome, int telemovel) {
        return listaCandidaturas.createCandidatura(morada, nome, telemovel);
    }  
    
    /**
     * Devolve a descrição textual do evento.
     *
     * @return descrição textual do evento
     */
    @Override
    public String toString() {
        return titulo;
    }
}
