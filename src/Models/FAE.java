package Models;

import java.io.Serializable;

public class FAE implements Serializable {

    /**
     * Utilizador definido como FAE.
     */
    private Utilizador utilizador;
    
    /**
     * Experiência do FAE.
     */
    private int experiencia;

    /**
     * A carga de trabalho do FAE.
     */
    private int carga;
    
    /**
     * Utilizador definido como FAE, por omissão.
     */
    private final Utilizador UTILIZADOR_POR_OMISSAO = new Utilizador();
    
    /**
     * Experiência inicial do FAE, por omissão.
     */
    private final static int EXPERIENCIA_INICIAL = 0;
    
    /**
     * Carga inicial do FAE, por omissão.
     */
    private final int CARGA_POR_OMISSAO = 0;

    /**
     * Inicia uma instância da classe FAE, sem argumentos.
     */
    public FAE() {
        this.utilizador = UTILIZADOR_POR_OMISSAO;
        this.experiencia = EXPERIENCIA_INICIAL;
        this.carga = CARGA_POR_OMISSAO;
    }

    /**
     * Inicia uma instância da classe FAE, enviando o utilizador como argumento.
     * @param utilizador utilizador recebido.
     */
    public FAE(Utilizador utilizador) {
        setUtilizador(utilizador);
        this.experiencia = EXPERIENCIA_INICIAL;
        this.carga = CARGA_POR_OMISSAO;
    }
    
    /**
     * Inicia uma instância da classe FAE, enviando o utilizador, experiência e carga como argumentos.
     * @param utilizador utilizador enviado por parâmetro
     * @param experiencia experiência do FAE
     * @param carga carga do FAE
     */
    public FAE(Utilizador utilizador, int experiencia, int carga) {
        setUtilizador(utilizador);
        this.experiencia = experiencia;
        this.carga = carga;
    }

    /**
     * Método que devolve o utilizador.
     * @return utilizador
     */
    public Utilizador getUtilizador() {
        return this.utilizador;
    }
    
    /**
     * Devolve a experiência do FAE.
     *
     * @return experiencia
     */
    public int getExperiencia() {
        return this.experiencia;
    }
    
    /**
     * Método que permite modificar o utilizador, recebendo o novo como parâmetro.
     * @param utilizador novo utilizador.
     */
    public final void setUtilizador(Utilizador utilizador) {
        if (utilizador == null) {
            throw new IllegalArgumentException("O Utilizador não existe");
        }
        this.utilizador = utilizador;
    }

    /**
     * Devolve a carga de trabalho do FAE.
     *
     * @return carga - a carga de trabalho do FAE
     */
    public int getCarga() {
        return carga;
    }
    
    /**
     * Incrementa a carga do FAE.
     */
    public void adicionaCarga() {
        this.carga++;
    }

    /**
     * Decrementa a carga do FAE.
     */
    public void retiraCarga() {
        this.carga--;
    }

    /**
     * Incrementa a experiência do FAE.
     */
    public void adicionaExperiencia() {
        this.experiencia++;
    }
    
    /**
     * Decrementa a experiência do FAE.
     */
    public void retiraExperiencia() {
        this.experiencia--;
    }

    /**
     * Método que valida o FAE.
     * @return boolean com resultado
     */
    public boolean valida(){
        return utilizador.valida();
    }
    
    /**
     * Método que compara dois FAE. 
     * @param obj FAE a comparar.
     * @return boolean com resultado da comparação
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof FAE) {
            FAE that = (FAE) obj;
            return that.utilizador.equals(this.utilizador);
        }
        return false;
    }

    /**
     * Devolve a descrição textual do FAE.
     * @return 
     */ 
    @Override
    public String toString() {
        return this.utilizador.getUsername();
    }
}