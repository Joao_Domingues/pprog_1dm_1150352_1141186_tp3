package Models;

import UI.AlgoritmoAtribuicaoUI;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RegistoAlgoritmos implements Serializable{
    
    /**
    * Lista que vai receber os Algoritmos de Atribuição.
    */
    private final List<AlgoritmoAtribuicaoUI> listaAlgoritmos;

    /**
    * Cria uma lista de Algoritmos.
    */
    public RegistoAlgoritmos() {
        listaAlgoritmos = new ArrayList<>();
    }

    /**
    * Metodo que retorna a lista de algoritmos
    * @return listaAlgoritmos
    */
    public List<AlgoritmoAtribuicaoUI> getMecanismos() {
        return listaAlgoritmos;
    }

    /**
     * Metodo que adiciona um algoritmo a lista de algoritmos; 
     * @param aa algoritmo de atribuição.
    */
    public void addAlgoritmo(AlgoritmoAtribuicaoUI aa) {
        listaAlgoritmos.add(aa);
    }   
}

