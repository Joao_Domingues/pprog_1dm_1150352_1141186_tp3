/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RegistoCandidaturas implements Serializable {
    
    /**
     * Lista de candidaturas.
     */
     private List<Candidatura> lstCandidaturas;
    
    /**
    * Método que cria um registo de candidaturas.
    */      
    public RegistoCandidaturas() {
        lstCandidaturas = new ArrayList<>();
    }
    
    /**
     * Método que cria um registo de candidaturas recebendo por parâmetro a lista de candidaturas.
     * 
     * @param lstCandidaturas lista de candidaturas
     */
    
    public RegistoCandidaturas(RegistoCandidaturas lstCandidaturas){
        this.lstCandidaturas = new ArrayList<>(lstCandidaturas.getListaCandidaturas());
    }

    /**
     * Acede à lista de candidaturas.
     * @return 
     */
    
    public List<Candidatura> getListaCandidaturas() {
        return new ArrayList(lstCandidaturas);
    }
    
    /**
     * Cria uma nova candidatura.
     *
     * @return Candidatura
     */
    public Candidatura novaCandidatura() {
        return new Candidatura();
    }
    
    /**
     * Permite obter uma candidatura.
     *
     * @param ID ID
     * @return candidatura
     */
    
    public Candidatura obterCandidatura(int ID) {
        return getListaCandidaturas().get(ID);
    }
    
    /**
     * Regista uma nova candidatura.
     * @param candidatura nova candidatura a registar.
     * @return 
     */
    
    public boolean registarCandidatura(Candidatura candidatura) {
        return lstCandidaturas.contains(candidatura)
                ? false
                : lstCandidaturas.add(candidatura);
    }
    
    public int adicionarListaCandidaturas (RegistoCandidaturas lstCandidaturas){
        int totalCandidaturasAdicionadas = 0;
        for (Candidatura candidatura : lstCandidaturas.getListaCandidaturas()) {
            boolean candidaturaAdicionada = registarCandidatura(candidatura);
            if(candidaturaAdicionada){
                totalCandidaturasAdicionadas++;
            }
        }
        return totalCandidaturasAdicionadas;
    }
    
    /**
     * Permite saber o número de candidaturas existentes.
     * @return 
     */
    
    public int tamanho(){
        return this.getListaCandidaturas().size();
    }
    
    /**
     * Permite remover uma candidatura da lista de candidaturas.
     * @param candidatura candidatura a remover.
     * @return 
     */
    public boolean removerCandidaturas(Candidatura candidatura){
        return lstCandidaturas.remove(candidatura);
    }    
}
