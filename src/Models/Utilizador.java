package Models;

import java.io.Serializable;

public class Utilizador implements Serializable{

    /**
     * Variável de instância que guarda o nome do utilizador.
     */
    private String nome;
    
    /**
     * Variável de instância que guarda o email do utilizador.
     */
    private String email;
    
    /**
     * Variável de instância que guarda a password do utilizador.
     */
    private String password;

    /**
     * Variável de instância que guarda o username do utilizador.
     */
    private String username;
    
    /**
     * Nome do utilizador por omissão.
     */
    private final String NOME_POR_OMISSAO = "Sem_Nome";
    
    /**
     * Email do utilizador por omissão.
     */
    private final String EMAIL_POR_OMISSAO = "Sem_Email";
    
    /**
     * Password do utilizador por omissão.
     */
    private final String PASSWORD_POR_OMISSAO = "Sem_Password";

    /**
     * Username do utilizador por omissão.
     */
    private final String USERNAME_POR_OMISSAO = "Sem_Username";
    
    /**
     * Constrói uma instância de Utilizador com o nome, email e password por omissão.
     */
    public Utilizador() {
        this.nome = NOME_POR_OMISSAO;
        this.email = EMAIL_POR_OMISSAO;
        this.password = PASSWORD_POR_OMISSAO;
        this.username = USERNAME_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Utilizador recebendo o nome, o email e password.
     *
     * @param nome o nome do utilizador.
     * @param email o email do utilizador.
     * @param password a password do utilizador.
     * @param username o username do utilizador
     */

    public Utilizador(String nome, String email, String password, String username) {
        this.nome = nome;
        this.email = email;
        this.password = password;
        this.username = username;
    }

    /**
     * Método que acede ao nome do utilizador.
     *
     * @return nome do utilizador.
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Método que altera o nome do utilizador, recebendo um novo por parâmetro.
     *
     * @param nome novo nome.
     */
    public void setNome(String nome) {
        if (nome == null || nome.equals("")) {
            throw new IllegalArgumentException("Nome inválido");
        }
        this.nome = nome;
    }
    
    /**
     * Método que acede ao email do utilizador.
     *
     * @return email do utilizador.
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Método que altera o email do utilizador, recebendo um novo por parâmetro.
     *
     * @param email novo email.
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * Método que acede à password do utilizador.
     *
     * @return password do utilizador.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Método que altera a password do utilizador, recebendo uma nova por parâmetro.
     *
     * @param password nova password.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Método que acede ao username do utilizador.
     *
     * @return username do utilizador.
     */
    public String getUsername() {
        return this.username;
    }
    
    /**
     * Método que permite modificar o username do utilizador, recebendo o novo
     * como parâmetro.
     *
     * @param username novo username
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * Método que valida a password do utilizador.
     *
     * @param password password do utilizador
     * @return boolean com resultado da validação
     */
    public static boolean validaPassword(String password) {
        if (password.isEmpty()) {
            return false;
        }
        return password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[,.-;:])(?!.*\\s).{4,7}$");
    }
    
    /**
     * Método que valida o nome do utilizador.
     *
     * @return boolean com resultado da validação
     */
    private boolean validaNome() {
        if (nome.isEmpty()) {
            return false;
        }
        String specialChars = "\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF";
        return nome.matches("[a-z 0-9A-Z" + specialChars + "]*");
    }
    
    /**
     * Método que valida o email do utilizador.
     *
     * @return boolean com resultado da validação
     */
    private boolean validaEmail() {
        if (email.isEmpty()) {
            return false;
        }
        return email.matches("(([a-z0-9!#$%&'*+-/=?^_`{|}~]+)|([a-z0-9!#$%&'*+-/=?^_`{|}~]+.[a-z0-9!#$%&'*+-/=?^_`{|}~]+))@[a-z]+([.][a-z]+)+");
    }
    
    /**
     * Método que valida o username do utilizador.
     *
     * @return boolean com resultado da validação
     */
    private boolean validaUsername() {
        if (username.isEmpty()) {   
            return false;
        }
        return username.matches("[a-zA-Z0-9_@.]{4,}");
    }
    
    /**
     * Método que valida o nome, email e username do utilizador.
     *
     * @return boolean com resultado da validação
     */
    public boolean valida() {
        return validaNome() && validaEmail() && validaUsername();
    }
    
    /**
     * Método que apresenta a informação do utilizador.
     *
     * @return dados do utilizador.
     */
    public String getInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nome: ").append(nome).append("\n");
        sb.append("Username: ").append(username).append("\n");
        sb.append("Email: ").append(email).append("\n");
        return sb.toString();
    }
    
    /**
     * Verifica a unicidade do utilizador.
     * @param other
     * @return 
     */

    @Override
    public boolean equals(Object other) {
        boolean result = other != null && getClass().equals(other.getClass());
        if (result) {
            Utilizador o = getClass().cast(other);
            result = (this == other) || (email.equals(o.email)) || (username.equals(o.username));
        }
        return result;
    }
 
    /**
     * Devolve a descrição textual do utilizador.
     *
     * @return
     */
    @Override
    public String toString() {
        return this.username;
    }
}
