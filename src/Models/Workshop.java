
package Models;

import java.util.ArrayList;
import java.util.List;

public class Workshop {
    
    /**
     * Código de identificação do Workshop.
     */
    private String codigo;
    
    /**
     * Descrição do Workshop.
     */
    private String descricao;
    
    /**
     * Lista de Temas tratados no Workshop.
     */
    private List<String> listaTemas;
    
    /**
     * Código de identificação do Workshop, por omissão.
     */
    private final String CODIGO_POR_OMISSAO = "Sem_Codigo";
    
    /**
     * Descrição do Workshop, por omissão.
     */
    private final String DESCRICAO_POR_OMISSAO = "Sem_Descricao";
    
    /**
     * Cria uma instância da classe Workshop, com atributos por omissão.
     */
    public Workshop() {
        this.codigo = CODIGO_POR_OMISSAO;
        this.descricao = DESCRICAO_POR_OMISSAO;
        this.listaTemas = new ArrayList<>();
    }    
}
