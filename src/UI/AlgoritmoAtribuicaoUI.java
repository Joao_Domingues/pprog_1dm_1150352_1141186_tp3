package UI;

import Models.Atribuicao;
import Models.Evento;
import java.util.List;

public interface AlgoritmoAtribuicaoUI {

    public List<Atribuicao> criarAtribuicoes(Evento evento);

    /**
     * Método que permite modificar a descrição do algoritmo de atribuição, recebendo a nova como parâmetro.
     * @param descricao nova descrição do algoritmo
     */
    public void setDescricao(String descricao);

    /**
     * Método que devolve a descrição do algoritmo de atribuição.
     * @return 
     */
    public String getDescricao();
}