/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Models.Atribuicao;
import Models.Evento;
import java.util.List;

public interface AlgoritmosAtribuicao {
    
    /**
     * Permite efetuar uma atribuição num determinado evento e no qual deverá ser
     * definido um algoritmo próprio de atribuição de candidaturas.
     *
     * @param e evento.
     * @return atribuição
     */
    public List<Atribuicao> atribuir(Evento e);    
}
