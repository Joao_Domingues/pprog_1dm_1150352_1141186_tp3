 
package UI;

import Models.AlgoritmoAtribuicao;
import Models.Atribuicao;
import Models.Candidatura;
import Models.Evento;
import Models.FAE;
import java.util.List;

public interface AtribuirCandidaturaUI {
    
    public List<Evento> getListaEventos();
    
    public void selectEvento(Evento evento);
    
    public List<AlgoritmoAtribuicao> getListaAlgoritmos();
    
    public List<Candidatura> getListaCandidaturas();
    
    public List<FAE> getListaFAE();
    
    public void selectAlgoritmo(AlgoritmoAtribuicao algoritmo);
    
    public List<Atribuicao> createListaAtribuicoes();
    
    public boolean validaAtribuicoes();
    
    public void addListaAtribuicoes();
}
