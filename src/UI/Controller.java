
package UI;

import Models.Evento;
import java.util.List;

public interface Controller {

    /**
     * Lista de eventos associados ao contexto.
     *
     * @return eventos
     */
    public List<Evento> listarEventos();

    /**
     * Permite definir um evento.
     *
     * @param evento evento a definir
     */
    public void definirEvento(Evento evento);
}
