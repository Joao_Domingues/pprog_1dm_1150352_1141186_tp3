
package UI;

import Models.Candidatura;
import Models.Evento;
import java.util.List;

public interface DecidirCandidaturaUI {

    public List<Evento> getListaEventos();

    public void selectEvento(Evento e);

    public List<Candidatura> getListaCandidaturasFAE();

    public void selectCandidatura(Candidatura candidatura);

    public void createDecisao(boolean decisao, String textoDescritivo);

    public boolean validaDecisao();

    public void addDecisao();
}
