/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import GUI.JanelaPrincipal;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Evento;
import Models.FAE;
import Models.Organizador;
import Models.Utilizador;
import Utils.Descodificador;

public class Main {

    /**
     * @param args [NO ARGS]
     */
    public static void main(String[] args) {
        final String WINDOW_TITLE_BAR = "Instituto Superior de Engenharia do Porto (ISEP)";
        CentroEventos cde = initialization();
        
//        CentroEventos cde = new CentroEventos();
//
//        Utilizador util = new Utilizador("joao", "joao@gmail.com", "123", "joao1");
//        Utilizador util2 = new Utilizador("ze", "ze@gmail.com", "123", "ze1");
//        Utilizador util3 = new Utilizador("miguel", "ze@gmail.com", "123", "miguel1");
//        Utilizador util4 = new Utilizador("bino", "ze@gmail.com", "123", "bino1");
//        Utilizador util5 = new Utilizador("beto", "ze@gmail.com", "123", "beto1");
//        cde.getListaUtilizadores().add(util);
//        cde.getListaUtilizadores().add(util2);
//        cde.getListaUtilizadores().add(util3);
//        cde.getListaUtilizadores().add(util4);
//        cde.getListaUtilizadores().add(util5);
//        
//        Evento evento1 = new Evento("Evento1", "Texto Descritivo", "2017-06-20-2017-06-25", "ISEP");
//        Evento evento2 = new Evento("Evento2", "Texto Descritivo", "2017-06-20-2017-06-25", "ISEP");
//
//        FAE fae = new FAE(util);
//        FAE fae1 = new FAE(util2);
//        FAE fae2 = new FAE(util3);
//        FAE fae3 = new FAE(util4);
//        FAE fae4 = new FAE(util5);
//        evento1.getRegistoFAE().registarFAE(fae);
//        evento1.getRegistoFAE().registarFAE(fae1);
//        evento1.getRegistoFAE().registarFAE(fae2);
//        evento1.getRegistoFAE().registarFAE(fae3);
//        evento1.getRegistoFAE().registarFAE(fae4);
//        
//        Candidatura c1 = new Candidatura("morada1", "Candidatura_1", 918969185);
//        Candidatura c2 = new Candidatura("morada2", "Candidatura_2", 962548053);
//        Candidatura c3 = new Candidatura("morada3", "Candidatura_3", 964775265);
//        Candidatura c4 = new Candidatura("morada4", "Candidatura_4", 966566768);
//        evento1.getListaCandidaturas().add(c1);
//        evento1.getListaCandidaturas().add(c2);
//        evento1.getListaCandidaturas().add(c3);
//        evento1.getListaCandidaturas().add(c4);
//        
//        evento1.getListaOrganizadores().registarOrganizador(new Organizador(util));
//        evento2.getListaOrganizadores().registarOrganizador(new Organizador(util));
//        
//        cde.getListaEventos().add(evento1);
//        cde.getListaEventos().add(evento2);
        
        JanelaPrincipal window = new JanelaPrincipal(WINDOW_TITLE_BAR, cde);
    }

    /**
     * Initialization and create instances for the program run
     */
    private static CentroEventos initialization() {
        return Descodificador.readObject(Descodificador.NAME_OF_DEFAULT_FILE_DATA);
    }   
}