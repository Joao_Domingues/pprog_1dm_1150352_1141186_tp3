/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controllers.RegistarUtilizadorController;
import Models.CentroEventos;
import Models.Utilizador;
import java.util.Scanner;

public class RegistarUtilizadorUI {
    
    /**
     * Variável de acesso ao controller do registo de utilizadores.
     */
    private final RegistarUtilizadorController controller;
    
    /**
     * Variável que armazena o utilizador.
     */
    public final Utilizador utilizador;
    
    /**
     * Variável de leitura.
     */
    Scanner ler;
    
    /**
     * Método que permite o registo de um utilizador recebendo por parâmetro o centro de eventos e o utilizador.
     * @param ce centro de eventos.
     * @param u utilizador.
     */
    public RegistarUtilizadorUI(CentroEventos ce, Utilizador u) {
        controller = new RegistarUtilizadorController(ce);
        utilizador=u;
        ler = new Scanner(System.in);
    }
    
    /**
     * Método que permite a inicialização do registo de um utilizador.
     */
    public void run() {
        System.out.println("*** Registar Utilizador ***: ");

        do {
            controller.novoUtilizador();
            introduzDados();
            
            System.out.print("Continuar (s/n)?");
        }while ("s".equalsIgnoreCase(ler.nextLine()));      
    }
    
    /**
     * Método que permite a introdução dos dados para a criação de um utilizador.
     */
    public void introduzDados() {
        String nome, email, username, password;
        System.out.println("*** Introduza os dados pedidos ***: ");
        System.out.print("Nome    :"); nome = ler.nextLine();
        System.out.print("Email   :"); email = ler.nextLine();
        System.out.print("Username:"); username = ler.nextLine();
        System.out.print("Password:"); password = ler.nextLine();

        if(controller.setDados(nome, email, username, password)) {
            System.out.println(controller.toString());
            System.out.print("Confirma (s/n)?");
            
            if("s".equalsIgnoreCase(ler.nextLine())) {
                if( controller.registarUtilizador())
                    System.out.println("Operação efetuada com sucesso");
                else
                    System.out.println("ERRO: FALHOU VALIDAÇÃO GLOBAL!");
            }
        }
        else
            System.out.println("ERRO: FALHOU VALIDAÇÃO LOCAL!");
    }   
}
