
package UI;

import Models.Evento;
import java.util.List;

public interface SubmeterCandidaturaUI {
    
    public List<Evento> getListaEventos();
    
    public void selectEvento(Evento evento);
    
    public void getUtilizador(String email);
    
    public void setDados(String nome, String morada, int telemovel);
    
    public void addCandidatura();
}
