/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

public interface TabChangeListener {
    
    /**
    * Ativa-se quando um painel de separadores muda o seu separador para este TabChangeListener.
    */
    public void onTabChanged(); 
}
