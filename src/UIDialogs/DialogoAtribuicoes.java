/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIDialogs;

import Controllers.AtribuirCandidaturaController;
import Exceptions.IllegalAccessCandidaturasPorAtribuir;
import GUI.JanelaPrincipal;
import Models.Atribuicao;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DialogoAtribuicoes extends JDialog {

    private JPanel topPanel;
    private JPanel centerPanel;
    private JPanel bottomPanel;
    private JLabel lblNomeAlgoritmo;
    private JButton btnAccept;
    private JButton btnCancel;
    
    //GLOBAL CONSTANTS
    /**
     * Janela Principal.
     */
    private final Frame mainFrame;

    /**
     * Lista Atribuicoes Geradas.
     */
    private List<Atribuicao> listaAtribuicoes;

    /**
     * Controller do caso de uso.
     */
    private AtribuirCandidaturaController ctrl;

    /**
     * Janela default.
     */
    private final LayoutManager WINDOW_DEFAULT_LAYOUT = new BorderLayout(10, 10);

    /**
     * Construtor da janela de diálogo.
     *
     * @param mainFrame janela principal
     * @param title título
     * @param type
     * @param ctrl
     */
    public DialogoAtribuicoes(Frame mainFrame, String title, boolean type, AtribuirCandidaturaController ctrl) {
        super(mainFrame, title, type);
        this.mainFrame = mainFrame;
        this.ctrl = ctrl;

        preSetupFrame();
        initializeComponents();
        posSetupFrame();
    }

    /**
     * Setup da janela de diálogo antes da inicialização dos componentes. 
     */
    private void preSetupFrame() {
        setLayout(WINDOW_DEFAULT_LAYOUT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    /**
     * Setup da janela de diálogo após a inicialização dos componentes.
     */
    private void posSetupFrame() {
        getRootPane().setDefaultButton(btnAccept);
        pack();
        setMinimumSize(getSize());
        setResizable(false);
        setLocationRelativeTo(mainFrame);
        setVisible(true);
    }

    /**
     * Cria, inicializa e prepara as componentes.
     */
    private void initializeComponents() {
        this.listaAtribuicoes = ctrl.createListaAtribuicoes();
        String[] atrs = new String[listaAtribuicoes.size()];
        int i = 0;
        for (Atribuicao atr : listaAtribuicoes) {
            atrs[i] = atr.toString();
            i++;
        }
        //Containers of components
        topPanel = new JPanel();

        centerPanel = new JPanel(new GridLayout(listaAtribuicoes.size(), 1));

        bottomPanel = new JPanel();

        //Components and Layers
        lblNomeAlgoritmo = new JLabel("Algoritmo: " + this.ctrl.getDesignacaoAlgoritmo());

        for (String a : atrs) {
            centerPanel.add(new JLabel(a));
        }

        btnAccept = new JButton("Aceitar");
        btnAccept.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnAccept();
            }
        });
        btnAccept.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if (ke.equals(KeyEvent.VK_ENTER)) {
                    clickBtnAccept();
                }
            }

        });

        btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnCancel(event);
            }
        });

        //Adding the Components on the panels
        topPanel.add(lblNomeAlgoritmo);

        bottomPanel.add(btnCancel);
        bottomPanel.add(btnAccept);

        //Adding the panels/Components on the content pane
        add(topPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Permite submeter as atribuições efetuadas.
     *
     * @param event
     */
    private void clickBtnAccept() {
        if (!ctrl.validaAtribuicoes()) {
            throw new IllegalAccessCandidaturasPorAtribuir("As atribuições geradas já existem");
        }
        ctrl.addListaAtribuicoes();
        JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_MENU);
        dispose();
    }

    /**
     * Permite fechar a janela.
     *
     * @param event
     */
    private void clickBtnCancel(ActionEvent event) {
        dispose();
    }
}
