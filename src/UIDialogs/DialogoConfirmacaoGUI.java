/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIDialogs;

import Controllers.SubmeterCandidaturaController;
import GUI.DialogoSubmeterCandidaturaGUI;
import GUI.JanelaPrincipal;
import Models.Candidatura;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class DialogoConfirmacaoGUI extends JDialog {

    private JPanel bottomPanel;
    private JPanel centerPanel;
    private JPanel layerName;
    private JPanel layerAddress;
    private JPanel layerCellPhoneNumber;
    private JLabel lblName;
    private JLabel lblAddress;
    private JLabel lblCellPhoneNumber;
    private JTextField tfInputName;
    private JTextField tfInputAddress;
    private JTextField tfInputCellPhoneNumber;
    private JButton btnConfirm;
    private JButton btnCancel;

    //GLOBAL CONSTANTS
    /**
     * Janela Principal.
     */
    private final Frame mainFrame;
    
    /**
     * Controller do caso de uso.
     */
    private final SubmeterCandidaturaController ctrl;
    
    /**
     * Janela default.
     */
    private final LayoutManager WINDOW_DEFAULT_LAYOUT = new BorderLayout();
    
    /**
     * Cor da janela por default.
     */
    private final Color WINDOW_DEFAULT_BACKGROUND_COLOR = JanelaPrincipal.WINDOW_DEFAULT_BACKGROUND_COLOR;

    /**
     * Construtor da janela de diálogo.
     *
     * @param mainFrame janela principal
     * @param title título
     * @param type
     * @param ctrl Controller do caso de uso
     */
    public DialogoConfirmacaoGUI(Frame mainFrame, String title, boolean type, SubmeterCandidaturaController ctrl) {
        super(mainFrame, title, type);
        this.mainFrame = mainFrame;
        this.ctrl = ctrl;

        preSetupFrame();
        initializeComponents();
        posSetupFrame();
    }

    /**
     * Setup da janela de diálogo antes da inicialização dos componentes. 
     */
    private void preSetupFrame() {
        setLayout(WINDOW_DEFAULT_LAYOUT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    /**
     * Setup da janela de diálogo após a inicialização dos componentes.
     */
    private void posSetupFrame() {
        getRootPane().setDefaultButton(btnConfirm);
        pack();
        setMinimumSize(getSize());
        setResizable(false);
        setLocationRelativeTo(mainFrame);
        setVisible(true);
    }

    /**
     * Cria, inicializa e prepara as componentes.
     */
    private void initializeComponents() {
        Candidatura candidatura = ctrl.mostraCandidatura();

        centerPanel = new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        centerPanel.setBackground(WINDOW_DEFAULT_BACKGROUND_COLOR);

        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        bottomPanel.setBackground(WINDOW_DEFAULT_BACKGROUND_COLOR);
        //Components and Layers
        final Dimension DIMENSION_TEXT_FIELDS = new Dimension(140, 30);
        final Dimension DIMENSION_LISTS = new Dimension(140, 100);
        final int HORIZONTAL_GAP = 20, VERTICAL_GAP = 20;
        final LayoutManager LAYOUT_LAYER_FORM = new FlowLayout(FlowLayout.TRAILING, HORIZONTAL_GAP, VERTICAL_GAP);

        layerName = new JPanel(LAYOUT_LAYER_FORM);
        lblName = new JLabel("Nome:");
        tfInputName = new JTextField(candidatura.getNome());
        tfInputName.setEditable(false);
        tfInputName.setPreferredSize(DIMENSION_TEXT_FIELDS);
        tfInputName.setToolTipText(candidatura.getNome());
        layerName.add(lblName);
        layerName.add(tfInputName);
        layerName.setBackground(null);

        layerAddress = new JPanel(LAYOUT_LAYER_FORM);
        lblAddress = new JLabel("Morada:");
        tfInputAddress = new JTextField(candidatura.getMorada());
        tfInputAddress.setPreferredSize(DIMENSION_TEXT_FIELDS);
        tfInputAddress.setEditable(false);
        tfInputAddress.setToolTipText(candidatura.getMorada());
        layerAddress.add(lblAddress);
        layerAddress.add(tfInputAddress);
        layerAddress.setBackground(null);

        layerCellPhoneNumber = new JPanel(LAYOUT_LAYER_FORM);
        lblCellPhoneNumber = new JLabel("Telemovel:");
        tfInputCellPhoneNumber = new JTextField(Integer.toString(candidatura.getTelemovel()));
        tfInputCellPhoneNumber.setEditable(false);
        tfInputCellPhoneNumber.setPreferredSize(DIMENSION_TEXT_FIELDS);
        tfInputCellPhoneNumber.setToolTipText(Integer.toString(candidatura.getTelemovel()));
        layerCellPhoneNumber.add(lblCellPhoneNumber);
        layerCellPhoneNumber.add(tfInputCellPhoneNumber);
        layerCellPhoneNumber.setBackground(null);

        btnConfirm = new JButton("Guardar");
        btnConfirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnConfirm(event);
            }
        });

        btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnCancel(event);
            }
        });

        //Adding the Components on the panels
        centerPanel.add(layerName);
        centerPanel.add(layerAddress);
        centerPanel.add(layerCellPhoneNumber);

        bottomPanel.add(btnCancel);
        bottomPanel.add(btnConfirm);

        //Adding the panels/Components on the content pane
        //Adding the Components on the panels
        bottomPanel.add(btnCancel);

        //Adding the panels/Components on the content pane
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

     /**
     * Botão de confirmação.
     *
     * @param event
     */
    private void clickBtnConfirm(ActionEvent event) {
//        ctrl.registaCandidatura();
//        ctrl.validaCandidatura();
        ctrl.addCandidatura();
        ((DialogoSubmeterCandidaturaGUI) JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_USE_CASE_5)).defaultForm();
        JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_MENU);
        dispose();
    }

    /**
     * Permite fechar a janela.
     *
     * @param event
     */
    private void clickBtnCancel(ActionEvent event) {
        dispose();
    }
}
