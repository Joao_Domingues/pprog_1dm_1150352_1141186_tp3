/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIDialogs;

import Controllers.Controller;
import GUI.JanelaPrincipal;
import Models.Evento;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class DialogoEscolhaEventosGUI extends JDialog {

    //Componentes
    private JPanel topPanel;
    private JPanel centerPanel;
    private JPanel bottomPanel;
    private JList<Evento> listaEventos;
    private JButton btnAccept;
    private JButton btnCancel;

    /**
     * Janela Principal.
     */
    private final Frame janelaPrincipal;
    
    /**
     * Controller do caso de uso.
     */
    private final Controller ctrl;
    
    /**
     * Janeça default.
     */
    private final LayoutManager WINDOW_DEFAULT_LAYOUT = new BorderLayout();

    /**
     * Construtor da janela de diálogo.
     *
     * @param janelaPrincipal janela principal
     * @param title título
     * @param type
     * @param ctrl Controller do caso de uso
     */
    public DialogoEscolhaEventosGUI(Frame janelaPrincipal, String title, boolean type, Controller ctrl) {
        super(janelaPrincipal, title, type);
        this.janelaPrincipal = janelaPrincipal;
        this.ctrl = ctrl;

        preSetupFrame();
        initializeComponents();
        posSetupFrame();
    }

    /**
     * Setup da janela de diálogo antes da inicialização dos componentes. 
     */
    private void preSetupFrame() {
        setLayout(WINDOW_DEFAULT_LAYOUT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    /**
     * Setup da janela de diálogo após a inicialização dos componentes.
     */
    private void posSetupFrame() {
        getRootPane().setDefaultButton(btnAccept);
        pack();
        setMinimumSize(getSize());
        setResizable(false);
        setLocationRelativeTo(janelaPrincipal);
        setVisible(true);
    }

    /**
     * Cria, inicializa e prepara as componentes.
     */
    private void initializeComponents() {
        //Containers of components
        centerPanel = new JPanel();

        bottomPanel = new JPanel();

        //Components and Layers
        listaEventos = new JList<>();
        listaEventos.setModel(new AbstractListModel<Evento>() {
            List<Evento> list = ctrl.getListaEventos();

            @Override
            public int getSize() {
                return list.size();
            }

            @Override
            public Evento getElementAt(int i) {
                return list.get(i);
            }
        });
        listaEventos.setBackground(null);
        listaEventos.setBorder(new TitledBorder("Eventos"));

        btnAccept = new JButton("Aceitar");
        btnAccept.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnAccept(event);
            }
        });
        btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnCancel(event);
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                exit();
            }

        });

        //Adding the Components on the panels
        centerPanel.add(listaEventos);

        bottomPanel.add(btnCancel);
        bottomPanel.add(btnAccept);

        //Adding the panels/Components on the content pane
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Botão de confirmação.
     *
     * @param event
     */
    private void clickBtnAccept(ActionEvent event) {
        try {
            Evento e = listaEventos.getSelectedValue();
            ctrl.selectEvento(e);
            dispose();
        } catch (IllegalAccessError ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

   /**
     * Permite cancelar as ações.
     *
     * @param event
     */
    private void clickBtnCancel(ActionEvent event) {
        exit();
    }

    /**
     * Permite fechar a janela.
     *
     * @param event
     */
    private void exit() {
        JanelaPrincipal.changePanel(JanelaPrincipal.INDEX_PANEL_MENU);
        dispose();
    }
}
