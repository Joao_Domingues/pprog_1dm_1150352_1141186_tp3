/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIDialogs;

import Models.CentroEventos;
import Utils.Descodificador;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * JFileChooser that configure the import file system
 */
public class DialogoEscolhaFicheiro extends JFileChooser {

    /**
     * Constructor
     * Construtor da janela de diálogo sem parâmetros.
     */
    public DialogoEscolhaFicheiro() {
        setup();
    }

    /**
     * Setup do JFileChooser.
     */
    private void setup() {
        setMultiSelectionEnabled(false);
        setFileFilter(new FileNameExtensionFilter("Text Files (*.txt)", "txt"));
        setCurrentDirectory(new File(System.getProperty("user.dir")));
    }

    /**
     * Quando o ficheiro é selecionado, o JFileChooser lê o ficheiro de texto e escreve um objeto que foi
     * inicializado a partir de um método na classe Descodificador e escreve-o num ficheiro binário.
     */
    public void saveFileInitializer() {
        File file = getSelectedFile();
        if (!file.getName().contains(".txt")) {
            throw new IllegalArgumentException("O Ficheiro deve ser de texto (*.txt)");
        }
        //read the config file (.txt)
        CentroEventos importedObject = Descodificador.importFileInitializer(file);
        //write data on .bin
        Descodificador.writeObject(Descodificador.NAME_OF_DEFAULT_FILE_DATA, importedObject);
    }
}
