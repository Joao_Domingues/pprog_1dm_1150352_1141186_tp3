/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIDialogs;

import Models.CentroEventos;
import java.awt.Frame;
import javax.swing.AbstractListModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import Models.Utilizador;
import java.awt.BorderLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;

/**
 * JDialog that serves to select an Utilizador to make the login
 */
public class IniciarSessaoGUI extends JDialog {

    //GLOBAL COMPONENTS
    private JPanel centerPanel;
    private JPanel bottomPanel;
    private JList userList;
    private JButton btnOk;
    //GLOBAL CONSTANTES
    /**
     * Global static variable that tells the current session
     */
    public static Utilizador SelectedUser;
    /**
     * Main Frame
     */
    private final Frame mainFrame;
    /**
     * Centro de Exposicoes
     */
    private final CentroEventos cde;
    /**
     * Window default layout manager of contentPane
     */
    private final LayoutManager WINDOW_DEFAULT_LAYOUT = new BorderLayout();
    /**
     * Window default if is set visible or not
     */
    private final boolean WINDOW_DEFAULT_IS_VISIBLE = true;

    /**
     * Constructor of the dialog window
     *
     * @param mainFrame Main Frame
     * @param title title bar
     * @param type specifies whether dialog blocks user input to other top-level
     * windows when shown. If true, the modality type property is set to
     * DEFAULT_MODALITY_TYPE otherwise the dialog is modeless
     * @param cde CentroDeExposicoes
     */
    public IniciarSessaoGUI(Frame mainFrame, String title, boolean type, CentroEventos cde) {
        super(mainFrame, title, type);
        this.mainFrame = mainFrame;
        this.cde = cde;

        preSetupFrame();
        initializeComponents();
        posSetupFrame();
    }

    /**
     * Setup the ContentPane/window of dialog frame before the initialize the
     * components
     */
    private void preSetupFrame() {
        setLayout(WINDOW_DEFAULT_LAYOUT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                dispose();
            }
        });
    }

    /**
     * Setup the ContentPane/window of dialog frame after the initialize the
     * components
     */
    private void posSetupFrame() {
        getRootPane().setDefaultButton(btnOk);
        pack();
        setMinimumSize(getSize());
        setResizable(false);
        setLocationRelativeTo(mainFrame);
        setVisible(WINDOW_DEFAULT_IS_VISIBLE);
    }

    /**
     * Create, initialize and setup the components
     */
    private void initializeComponents() {
        //Containers of components
        centerPanel = new JPanel();

        bottomPanel = new JPanel();

        //Components and Layers
        userList = new JList(new AbstractListModel() {

            List<Utilizador> utilizadores = cde.getRegistoUtilizadores().getListaUtilizadores();

            @Override
            public int getSize() {
                return utilizadores.size();
            }

            @Override
            public Object getElementAt(int i) {
                return utilizadores.get(i);
            }
        });
        userList.setBackground(null);
        userList.setBorder(new TitledBorder("Utilizadores"));

        btnOk = new JButton("Selecionar");
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                clickBtnOk();
            }
        });
        btnOk.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if (ke.equals(KeyEvent.VK_ENTER)) {
                    clickBtnOk();
                }
            }

        });

        //Adding the Components on the panels
        centerPanel.add(userList);
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));

        bottomPanel.add(btnOk);

        //Adding the panels/Components on the content pane
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);

    }

    /**
     * Click button ok
     */
    private void clickBtnOk() {
        try {
            SelectedUser = (Utilizador) userList.getSelectedValue();
            if (SelectedUser == null) {
                throw new IllegalAccessException("Não foi selecionado um utilizador. \n"
                        + "Por favor, selecione um utilizador para efetuar corretamente o login.");
            }
            dispose();
        } catch (IllegalAccessException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
}
