
package Utils;

import Models.Candidatura;
import Models.CentroEventos;
import Models.Evento;
import Models.FAE;
import Models.Organizador;
import Models.Utilizador;
import UIDialogs.DialogoEscolhaFicheiro;
import java.io.File;
import javax.swing.JOptionPane;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import javax.swing.JFileChooser;

public class Descodificador {

    public static String NAME_OF_DEFAULT_FILE_DATA = "data-cde.bin";

    /** 
     * Escreve um objeto que foi passado por parâmetro para um ficheiro binário (*.bin).
     *
     * @param fileName nome do ficheiro
     * @param cde Centro de Eventos
     */
    public static void writeObject(String fileName, CentroEventos cde) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            try {
                out.writeObject(cde);
            } finally {
                out.close();
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Não foi possivel a escrita do ficheiro\n" + ex.getMessage(),
                    "Erro",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Lê os dados de um ficheiro binário para carregar o objeto para memória. Se o ficheiro binário não estiver pronto
     * para carregamento, o sistema irá perguntar ao utilizador se ele pretende ler de um ficheiro .txt já pronto.
     * Se sim, o sistema irá gerar um ficheiro binário para carregar o objeto para memória.
     *
     * @param fileName nome do ficheiro
     * @return Object que é lido
     */
    public static CentroEventos readObject(String fileName) {
        CentroEventos cde = null;
        boolean exception;
        do {
            exception = false;
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
                try {
                    cde = (CentroEventos) in.readObject();
                } finally {
                    in.close();
                }
            } catch (ClassNotFoundException | IOException ex) {
                exception = true;
                int option = JOptionPane.showConfirmDialog(null,
                        "Ficheiro de persistência de dados não encontrado."
                        + "\nDeseja inicializar através do ficheiro de configuração?",
                        "Erro de Leitura",
                        JOptionPane.YES_NO_OPTION);
                if (option == 1) { //NO_OPTION_DIALOG_FRAME
                    System.exit(1);
                }
                DialogoEscolhaFicheiro fc = new DialogoEscolhaFicheiro();
                boolean exceptionExtention;
                try {
                    exceptionExtention = false;
                    if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        fc.saveFileInitializer();
                    } else {    //CANCEL_OPTION_JCHOOSEFILE
                        System.exit(0);
                    }
                } catch (IllegalArgumentException exExtention) {
                    exceptionExtention = true;
                    JOptionPane.showMessageDialog(null, exExtention.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        } while (exception);
        return cde;
    }

    /**
     * Este mecanismo importa os dados de um ficheiro .txt já configurado e inicializa os objetos do Centro de Eventos.

     * @param file ficheiro de texto
     * @return um objeto que representa o Centro de Eventos
     */
    public static CentroEventos importFileInitializer(File file) {
        CentroEventos cde = new CentroEventos();
        try {
            Scanner r = new Scanner(file, "UTF-8");
            try {
                String line;
                while (r.hasNext()) {
                    line = getNextLine(r);
                    if (line.contains("-Utilizador")) {
                        userInitialization(line, cde);
                    } else if (line.contains("-Evento")) {
                        eventsInitialization(r, line, cde);
                    }
                }
            } finally {
                r.close();
            }
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Ficheiro de inicialização não existe", "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(null, "Ficheiro Corrompido (" + ex.getMessage() + ")", "ERRO", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        return cde;
    }

    /**
     * Método auxiliar que inicializa um utilizador a partir de um ficheiro de texto (.txt).
     *
     * @param line linha lida
     * @param cde Centro de Eventos
     */
    private static void userInitialization(String line, CentroEventos cde) {
        line = line.replace("\t-Utilizador{", "").replace("}", "");
        String[] attributes = line.split(", ");
        if (attributes.length == 4) {
            Utilizador u = new Utilizador(attributes[0].split("=")[1], attributes[1].split("=")[1], attributes[2].split("=")[1], attributes[3].split("=")[1]);
            cde.getListaUtilizadores().add(u);
        }
    }

    /**
     * Método auxiliar que inicializa um evento, candidatura, organizador e FAE a partir de um ficheiro de texto 
     * (.txt).
     *
     * @param r Scanner
     * @param line linha que foi lida
     * @param cde Centro de Eventos
     */
    private static void eventsInitialization(Scanner r, String line, CentroEventos cde) {
        Evento e;
        line = line.replace("-Evento{", "").replace("}", "").trim();
        String[] attributes = line.split(", ");
        if (attributes.length == 4) {
            e = new Evento(attributes[0].split("=")[1], attributes[1].split("=")[1], attributes[2].split("=")[1], attributes[3].split("=")[1]);
            line = getNextLine(r);
            while (line.contains("-Candidatura") || line.contains("-Organizador") || line.contains("-FAE")) {
                if (line.contains("-Candidatura")) {
                    line = line.replace("-Candidatura{", "").replace("}", "").trim();
                    attributes = line.split(", ");
                    if (attributes.length == 3) {
                        Candidatura c = new Candidatura(attributes[0].split("=")[1], attributes[1].split("=")[1], Integer.parseInt(attributes[2].split("=")[1]));
                        e.getListaCandidaturas().add(c);
                    }
                }
                if (line.contains("-Organizador")) {
                    line = line.replace("-Organizador{", "").replace("}", "").trim();
                    attributes = line.split(", ");
                    if (attributes.length == 1) {
                        Organizador o = new Organizador(cde.getRegistoUtilizadores().getUtilizador(attributes[0].split("=")[1]));
                        e.getListaOrganizadores().registarOrganizador(o);
                    }
                }
                if (line.contains("-FAE")) {
                    line = line.replace("-FAE{", "").replace("}", "").trim();
                    attributes = line.split(", ");
                    if (attributes.length == 1) {
                        FAE f = new FAE(cde.getRegistoUtilizadores().getUtilizador(attributes[0].split("=")[1]));
                        e.getRegistoFAE().registarFAE(f);
                    }
                }
                line = getNextLine(r);
            }
            cde.getListaEventos().add(e);
        }
    }

    /**
     * Linha de texto personalizada que ignora todas as linhas que contiverem "#", visto ser um comentário.
     * 
     * @param r Scanner
     * @return linha que foi lida
     */
    private static String getNextLine(Scanner r) {
        String line = "";
        while (r.hasNext()) {
            line = r.nextLine().trim();
            if (!line.contains("#")) {
                if (line.isEmpty()) {
                    line = "";
                }
                return line;
            }
        }
        return line;
    }
}
