/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import Models.CentroEventos;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FicheiroCentroEventos {
    
    public static final String NOME = "CentroEventos.bin";
    
    /**
     * Método que lê informação a partir de um ficheiro binário e retoma o estado do centro de eventos
     * quando este foi criado.
     * 
     * @param nomeFicheiro ficheiro binário.
     * @return centroEventos.
     */
    public CentroEventos ler(String nomeFicheiro) {
        CentroEventos centroEventos;
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(nomeFicheiro));
            try {
                centroEventos = (CentroEventos) in.readObject();
            } finally {
                in.close();
            }
            return centroEventos;
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }
    
    /**
     * Guarda o estado atual do centro de eventos para que, posteriormente, o estado do mesmo possa ser retomado.
     * 
     * @param nomeFicheiro nome do ficheiro a guardar.
     * @param centroEventos centroeventos.
     * @return true caso tenha exportado com sucesso, false caso contrário.
     * @throws IOException 
     */
    
    public boolean guardar(String nomeFicheiro, CentroEventos centroEventos) throws IOException {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(centroEventos);
            } 
            finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
